import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { JoyrideModule } from 'ngx-joyride';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './Components/login/login.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { AppService } from './Services/app.service';
import { LoginService } from './Services/login.service';
import { BalanceCheckComponent } from './Components/balance-check/balance-check.component';
import { TransactionService } from './Services/transaction.service';
import { CaptureCodesComponent } from './Components/capture-codes/capture-codes.component';
import { ProcCodeSetupComponent } from './Components/proc-code-setup/proc-code-setup.component';
import { HPSetupComponent } from './Components/hpsetup/hpsetup.component';
import { HistoryCLaimsComponent } from './Components/history-claims/history-claims.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    BalanceCheckComponent,
    CaptureCodesComponent,
    ProcCodeSetupComponent,
    HPSetupComponent,
    HistoryCLaimsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    JoyrideModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: LoginComponent },
      { path: 'Login', component: LoginComponent },
      { path: 'Dash', component: DashboardComponent },
      { path: 'balance-check', component: BalanceCheckComponent },
      { path: 'Capture-Codes', component: CaptureCodesComponent },
      { path: 'ProcCodeSetup', component: ProcCodeSetupComponent },
      { path: 'hp-setup', component: HPSetupComponent },
      { path: 'history', component: HistoryCLaimsComponent }
    ]),
    ClarityModule,
    BrowserAnimationsModule
  ],
  providers: [AppService, LoginService, TransactionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
