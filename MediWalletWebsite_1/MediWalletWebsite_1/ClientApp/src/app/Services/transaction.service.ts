import { Injectable,Inject } from '@angular/core';
import { LoginDetailsModel } from '../Models/LoginDetailsModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import * as uuid from 'uuid';
import { ValidationResultsModel } from '../Models/ValidationResultsModel';
import { EventLoggingModel } from '../Models/EventLoggingModel';
import { ClaimCodesModel } from '../Models/ClaimCodesModel';
import { History } from '../Models/history';
import { formatDate } from '@angular/common';
import { DefaultCodesModel } from '../Models/DefaultCodesModel';
import { Emails } from '../Models/emails';


@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private _balanceFail: boolean = false;
  private _busy: boolean = false;
  private _error: boolean = false;
  private _amount: string;
  private _balanceSuccess: boolean = false;
  private _loginDetails: LoginDetailsModel = new LoginDetailsModel();
  private _historyList: History[] = [];
  private _cardnumber: string;
  private _memberDetails: string;
  private _codeList: ClaimCodesModel[] = [];
  private _procDesc: ValidationResultsModel = new ValidationResultsModel();
  private _otpSend: ValidationResultsModel = new ValidationResultsModel();
  private _successTransaction: boolean = false;
  private _errorMessage: string;
  public showReference: boolean = false;
  private _drcRef: string = "";
  private _mediRef: string = "";
  private _diagdesc: ValidationResultsModel = new ValidationResultsModel();
  private _diagCode: string = "";
  private _groups: DefaultCodesModel[] = [];
  private _groupDetail: DefaultCodesModel[] = [];
  private _diagCodeFinale: string = '';
  private _historyClaims: ClaimCodesModel[] = [];
  private _orgHistoryClaims: ClaimCodesModel[] = [];
  private _historyClaimsDetail: ClaimCodesModel[] = [];
  private _openDetailModel: boolean = false;
  private _teethNo: boolean = false;
  private _openReferenceModel: boolean = false;
  private _totalMedi: string;
  private _showEmail: boolean = false;


  constructor(private _http: HttpClient, private _router: Router, @Inject('BASE_URL') private _baseUrl: string) { }

/*getter asn setter*/
  public get totalMedi(): string {
    return this._totalMedi;
  }
  public set totalMedi(value: string) {
    this._totalMedi = value;
  }
  public get openReferenceModel(): boolean {
    return this._openReferenceModel;
  }
  public set openReferenceModel(value: boolean) {
    this._openReferenceModel = value;
  }
  public get errorMessage(): string {
    return this._errorMessage;
  }
  public set errorMessage(value: string) {
    this._errorMessage = value;
  }
  public get mediRef(): string {
    return this._mediRef;
  }
  public set mediRef(value: string) {
    this._mediRef = value;
  }
  public get drcRef(): string {
    return this._drcRef;
  }
  public set drcRef(value: string) {
    this._drcRef = value;
  }
  public get balanceSuccess(): boolean {
    return this._balanceSuccess;
  }
  public set balanceSuccess(value: boolean) {
    this._balanceSuccess = value;
  }
  public get balanceFail(): boolean {
    return this._balanceFail;
  }
  public set balanceFail(value: boolean) {
    this._balanceFail = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get loginDetails(): LoginDetailsModel {
    return this._loginDetails;
  }
  public set loginDetails(value: LoginDetailsModel) {
    this._loginDetails = value;
  }
  public get cardnumber(): string {
    return this._cardnumber;
  }
  public set cardnumber(value: string) {
    this._cardnumber = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get amount(): string {
    return this._amount;
  }
  public set amount(value: string) {
    this._amount = value;
  }
  public get memberDetails(): string {
    return this._memberDetails;
  }
  public set memberDetails(value: string) {
    this._memberDetails = value;
  }
  public get codeList(): ClaimCodesModel[] {
    return this._codeList;
  }
  public set codeList(value: ClaimCodesModel[]) {
    this._codeList = value;
  }
  public get procDesc(): ValidationResultsModel {
    return this._procDesc;
  }
  public set procDesc(value: ValidationResultsModel) {
    this._procDesc = value;
  }
  public get otpSend(): ValidationResultsModel {
    return this._otpSend;
  }
  public set otpSend(value: ValidationResultsModel) {
    this._otpSend = value;
  }
  public get successTransaction(): boolean {
    return this._successTransaction;
  }
  public set successTransaction(value: boolean) {
    this._successTransaction = value;
  }
  public get historyList(): History[] {
    return this._historyList;
  }
  public set historyList(value: History[]) {
    this._historyList = value;
  }
  public get diagdesc(): ValidationResultsModel {
    return this._diagdesc;
  }
  public set diagdesc(value: ValidationResultsModel) {
    this._diagdesc = value;
  }
  public get diagCode(): string {
    return this._diagCode;
  }
  public set diagCode(value: string) {
    this._diagCode = value;
  }
  public get groups(): DefaultCodesModel[] {
    return this._groups;
  }
  public set groups(value: DefaultCodesModel[]) {
    this._groups = value;
  }
  public get groupDetail(): DefaultCodesModel[] {
    return this._groupDetail;
  }
  public set groupDetail(value: DefaultCodesModel[]) {
    this._groupDetail = value;
  }
  public get diagCodeFinale(): string {
    return this._diagCodeFinale;
  }
  public set diagCodeFinale(value: string) {
    this._diagCodeFinale = value;
  }
  public get historyClaims(): ClaimCodesModel[] {
    return this._historyClaims;
  }
  public set historyClaims(value: ClaimCodesModel[]) {
    this._historyClaims = value;
  }
  public get OrgHistoryClaims(): ClaimCodesModel[] {
    return this._orgHistoryClaims;
  }
  public set OrgHistoryClaims(value: ClaimCodesModel[]) {
    this._orgHistoryClaims = value;
  }
  public get historyClaimsDetail(): ClaimCodesModel[] {
    return this._historyClaimsDetail;
  }
  public set historyClaimsDetail(value: ClaimCodesModel[]) {
    this._historyClaimsDetail = value;
  }
  public get openDetailModel(): boolean {
    return this._openDetailModel;
  }
  public set openDetailModel(value: boolean) {
    this._openDetailModel = value;
  }
  public get teethNo(): boolean {
    return this._teethNo;
  }
  public set teethNo(value: boolean) {
    this._teethNo = value;
  }
  public get showEmail(): boolean {
    return this._showEmail;
  }
  public set showEmail(value: boolean) {
    this._showEmail = value;
  }

/*BalanceCheck with FeverTree Api*/
  BalanceCheck(data: boolean) {
    this.busy = true;
    this.balanceSuccess = false;
    this.balanceFail = false;
    this.loginDetails.uuid = uuid.v4().toUpperCase();

    this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/BalanceLookup",
      {
        BalanceID: this.loginDetails.uuid.toString(),
        CardOrIDNumber: this.cardnumber.toString() 
      },
      {
        headers: new HttpHeaders({
          "Authorization": "Basic NzkyOmRlbW8=",
          "Content-Type": "application/json",
        })
      }
      ).subscribe((results: any) => {
        let req = +this._amount;
        let avail = +results.Available;
        if (avail > req) {
          this.balanceSuccess = true;
           
        } else {
          this.balanceFail = true;
        }

        this.memberDetails = results.Name;
        let c: History = new History();
        if (data) {
          c.amount = req.toFixed(2);
          c.memb = this.cardnumber;
          c.success = this.balanceSuccess;
          let date: Date = new Date;
          c.time = formatDate(date, "HH:mm:ss", 'en-za', '+0200')

          this.historyList.push(c);
        }

      },
        (error) => {
          this._error = true;
          console.log(error);
          this._errorMessage = error.statusText + ' ' + ':' + error.message;
          this._busy = false;
        },
        () => {
          this.busy = false;
          let c: EventLoggingModel = new EventLoggingModel();

          c.provid = this.loginDetails.provID;
          c.member = this.cardnumber;
          c.amount = this.amount;
          c.eventDesc = "BalanceCheck";
          c.username = this.loginDetails.username;
          c.uuid = this.loginDetails.uuid;

          this.LogEvent(c);

        });
  }

/*Log event on database*/
  LogEvent(event: EventLoggingModel) {
    this._busy = true;
    console.log("Log data   ---");
    this._http.post(this._baseUrl + 'api/Transaction/LogEvent',
      event,
      {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    ).subscribe((results: ValidationResultsModel) => {
      this._busy = true;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
        console.log("Log data   --- Done");
      })
  }

/*Verify Procedure Code*/
  ValidateProcCode(c: ClaimCodesModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Transaction/ValidateProcCode', c
    ).subscribe((results: ValidationResultsModel) => {
      if (results.valid) {
        this.procDesc.extra = results.extra;
        this.procDesc.message = results.message;
        this.procDesc.valid = true;
      } else {
        this.procDesc.valid = false;
      }
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })
  }

/*Verify Diagnosis Code*/
  ValidateDiagCode(c: ClaimCodesModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Transaction/ValidateDiagCode', c
    ).subscribe((results: ValidationResultsModel) => {
      if (results.valid) {
        this.diagdesc.message = results.message;
        this.diagdesc.valid = true;
      } else {
        this.diagdesc.valid = false;
      }
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })
  }

/*Send OTP with FeverTree Api*/
  SendOtp() {
    console.log("sendOtp---");
    this.busy = true;
    this.loginDetails.uuid = uuid.v4().toUpperCase();

    this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/SendOTP",
      {
        TransactionID: this.loginDetails.uuid,
        CardOrIDNumber: this.cardnumber
      },
      {
        headers: new HttpHeaders({
          "Authorization": "Basic NzkyOmRlbW8=",
          "Content-Type": "application/json",
        })
      }
    ).subscribe((results: any) => {
      console.log(results);

    },
      (error) => {
        this._error = true;
        console.log(error);
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
        let c: EventLoggingModel = new EventLoggingModel();

        c.provid = this.loginDetails.provID;
        c.member = this.cardnumber;
        c.amount = this.amount;
        c.eventDesc = "OTPSend";
        c.username = this.loginDetails.username;
        c.uuid = this.loginDetails.uuid;

        this.LogEvent(c);

      });
    setTimeout((q) => {
      this.busy = false;
    console.log("sendOtp---Done");

    },200)
   
  }

/*Confirm OTP with FeverTree Api*/
  ConfirmOtp(otp: string) {
    console.log("Confirm Otp---");
    this.busy = true;
    this.loginDetails.uuid = uuid.v4().toUpperCase();

   this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/CheckOTP",
      {
        TransactionID: this.loginDetails.uuid,
        CardOrIDNumber: this.cardnumber,
        OTP: otp
      },
      {
        headers: new HttpHeaders({
          "Authorization": "Basic NzkyOmRlbW8=",
          "Content-Type": "application/json",
        })
      }
    ).subscribe((results: any) => {
      this.busy = true;
      this.otpSend.message = otp;
      this.otpSend.valid = results.Success;
      console.log(results);

    },
      (error) => {
        this._error = true;
        console.log(error);
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
       // this.busy = false;
        console.log("Confirm Otp---done");
        let c: EventLoggingModel = new EventLoggingModel();

        c.provid = this.loginDetails.provID;
        c.member = this.cardnumber;
        c.amount = this.amount;
        c.eventDesc = "OTPConfirmation";
        c.username = this.loginDetails.username;
        c.uuid = this.loginDetails.uuid;

        this.LogEvent(c);

      });
  }

/*Process Payment with FeverTree Api*/
  ProcessPayment(total: string) {
    console.log("Process Payment ---");
    this.busy = true;
    this.loginDetails.uuid = uuid.v4().toUpperCase();

    this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/ProcessTransaction",
      {
        TransactionID: this.loginDetails.uuid,
        CardOrIDNumber: this.cardnumber,
        Amount: total,
        OTP: this.otpSend.message
      },
      {
        headers: new HttpHeaders({
          "Authorization": "Basic NzkyOmRlbW8=",
          "Content-Type": "application/json",
        })
      }
    ).subscribe((results: any) => {
      this.busy = true;
      this.successTransaction = results.Success;
      this.mediRef = results.Reference;
      this.drcRef = this.loginDetails.uuid;
      this.showReference = this.successTransaction;

    },
      (error) => {
        this._error = true;
        console.log(error);
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
        console.log("Process Payment Done ---");
        let c: EventLoggingModel = new EventLoggingModel();

        c.provid = this.loginDetails.provID;
        c.member = this.cardnumber;
        c.amount = this.amount;
        c.eventDesc = "TransactionCompleted";
        c.username = this.loginDetails.username;
        c.uuid = this.loginDetails.uuid;

        this.LogEvent(c);
        this.SaveData();

      });
  }

/*Save Transacion on database*/
  SaveData() {
    console.log("Save data   ---");
    this._busy = true;
    this.codeList[0].id = this.loginDetails.uuid;
    this.codeList[0].user = this.loginDetails.username;
    this.codeList[0].membId = this.cardnumber;
    this.codeList[0].provid = this.loginDetails.provID;
    this.codeList[0].mediRef = this.mediRef;
    let c: string[] = this.memberDetails.split(',');
    if (c.length > 1) {
      this.codeList[0].name = c[1];
      this.codeList[0].surname = c[0];
    } else {
      this.codeList[0].name = c[0];
      this.codeList[0].surname = c[0];
    }

    this.codeList.forEach((el) => {
      if (el.qty !== undefined && el.total !== null && el.qty !== "") {
        el.qty = el.qty.toString();
      } else {
        el.qty = "1";
      }
     
      if (el.total !== undefined && el.total !== null && el.total !== "") {
        el.total = el.total.toString();
      } else {
        el.total = "0.00";
      }
      if (el.singleUnitAmount !== undefined && el.singleUnitAmount !== null && el.singleUnitAmount !== "") {
        el.singleUnitAmount = el.singleUnitAmount.toString();
      } else {
        el.singleUnitAmount = "0.00";
      }
      
     
    })

    

    this._http.post(this._baseUrl + 'api/Transaction/SaveData', this.codeList
    ).subscribe((results: ValidationResultsModel) => {
      this.busy = true;
      this.drcRef = results.message;
      this.openReferenceModel = this.successTransaction;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        console.log("Save data   --- Done");
        this.busy = false;
        this.codeList = [];
        //this.cardnumber = "";
        this.amount = "";
        this.memberDetails = "";
        this.showEmail = true;
       
      })
  }


  GetDefaultCodeGroups() {

    this._busy = true;
    this._http.get(this._baseUrl + 'api/Transaction/GetDefaultCodeGroups'
    ).subscribe((results: DefaultCodesModel[]) => {
      this.groups = [];

      results.forEach((el) => {
        this.groups.push(el);
      });

      this.groups = this.groups.sort((a, b) => a.heading.length > b.heading.length ? 1 : -1);
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      });
  }

  GetDefaultCodeDetails(data: DefaultCodesModel) {
    
    this.busy = true;
    this._http.post(this._baseUrl + 'api/Transaction/GetDefaultCodeDetails', data
    ).subscribe((results: DefaultCodesModel[]) => {
      this.groupDetail = [];

      results.forEach((el) => {
        this.groupDetail.push(el);

      });

      this.groupDetail.forEach((el) => {
        let c: ClaimCodesModel = new ClaimCodesModel();

        c.code = el.code;
        c.desc = el.desc;
        c.enterQty = el.enterQty;
        c.enterTooth = el.enterTooth;
        c.singleUnitAmount = el.price;
        if (c.enterQty) {
          c.qty = el.defaultQty.toString();
        }

        if (c.enterQty) {
          c.total = ((+c.qty) * (+c.singleUnitAmount)).toFixed(2);
        }

        this.codeList.push(c);
      });
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
      })
  }

  GetAllClaims(data: LoginDetailsModel) {
    this._busy = true;

    this._http.post(this._baseUrl + 'api/Transaction/GetAllClaims', data
    ).subscribe((results: ClaimCodesModel[]) => {
      this._historyClaims = [];

      this._historyClaims = results;
      this._orgHistoryClaims = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },

      () => {
        this._busy = false;
      });
  }

  GetClaimDetail(data: ClaimCodesModel) {
    this._busy = true;
    this._openDetailModel = false;
    this._http.post(this._baseUrl + 'api/Transaction/GetClaimDetail', data
    ).subscribe((results: ClaimCodesModel[]) => {
      this._historyClaimsDetail = [];

      this._historyClaimsDetail = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },

      () => {
        this._busy = false;
        this._openDetailModel = true;
      });
  }

  ValidateToothNo(data: ClaimCodesModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Transaction/ValidateToothNo', data
    ).subscribe((results: ValidationResultsModel) => {
      this._teethNo = !results.valid;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })


  }

  SendEmail(data: Emails) {
    this._busy = true;
    this._showEmail = false;
    this._http.post(this._baseUrl + 'api/Transaction/SendEmail', data
    ).subscribe((results: ValidationResultsModel) => {
    
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
        this._showEmail = false;
      },
      () => {
        this._busy = false;
        this._showEmail = false;
        this.cardnumber = "";
        this._router.navigateByUrl("/Dash");
      })
  }
}
