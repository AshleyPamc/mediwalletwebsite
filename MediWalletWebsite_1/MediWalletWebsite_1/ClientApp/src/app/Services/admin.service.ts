import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DefaultCodesModel } from '../Models/DefaultCodesModel';
import { ValidationResultsModel } from '../Models/ValidationResultsModel';
import { HpSetup } from '../Models/hp-setup';
import { LoginDetailsModel } from '../Models/LoginDetailsModel';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private _error: boolean = false;
  private _busy: boolean = false;
  private _errorMessage: string;
  private _listProcCodes: DefaultCodesModel[] = [];
  private _totalPages: number = 0;
  private _codeGroupList: DefaultCodesModel[] = [];
  private _detailCodeList: DefaultCodesModel[] = [];
  private _hpsetupList: HpSetup[] = [];








  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string) { }

/*Getters & Setters*/
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get errorMessage(): string {
    return this._errorMessage;
  }
  public set errorMessage(value: string) {
    this._errorMessage = value;
  }
  public get listProcCodes(): DefaultCodesModel[] {
    return this._listProcCodes;
  }
  public set listProcCodes(value: DefaultCodesModel[]) {
    this._listProcCodes = value;
  }
  public get totalPages(): number {
    return this._totalPages;
  }
  public set totalPages(value: number) {
    this._totalPages = value;
  }
  public get codeGroupList(): DefaultCodesModel[] {
    return this._codeGroupList;
  }
  public set codeGroupList(value: DefaultCodesModel[]) {
    this._codeGroupList = value;
  }
  public get detailCodeList(): DefaultCodesModel[] {
    return this._detailCodeList;
  }
  public set detailCodeList(value: DefaultCodesModel[]) {
    this._detailCodeList = value;
  }
  public get hpsetupList(): HpSetup[] {
    return this._hpsetupList;
  }
  public set hpsetupList(value: HpSetup[]) {
    this._hpsetupList = value;
  }

  GetAllProcedureCodes() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Admin/GetProcCodes'
    ).subscribe((results: DefaultCodesModel[]) => {
      this.listProcCodes = [];
      let count: number = 0;
      let page: number = 1;
      results.forEach((el) => {
        count++;
        el.page = page;
        this.listProcCodes.push(el);

        if (count == 10) {
          count = 0;
          page++;
        }

      });
      this.totalPages = page;
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => { this._busy = false; })
  }

  GetAllCodeGroups() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Admin/GetExistingCodeGroups'
    ).subscribe((results: DefaultCodesModel[]) => {
      this.codeGroupList = [];

      results.forEach((el) => {

        this.codeGroupList.push(el);


      });
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => { this._busy = false; })
  }

  GetGroupDetailsCode(data: DefaultCodesModel) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Admin/GetExistingCodeGroupsDetail', data
    ).subscribe((results: DefaultCodesModel[]) => {
      this.codeGroupList = [];

      results.forEach((el) => {

        this._detailCodeList.push(el);


      });
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
        this.GetAllProcedureCodes();
      })
  }


  DeleteGroup(data: DefaultCodesModel) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Admin/DeleteExistingCodeGroup', data
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
        this.GetAllCodeGroups();
      });
  }

  AddGroup(data: DefaultCodesModel) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Admin/AddNewCode', data
    ).subscribe((results: ValidationResultsModel) => {
      this._detailCodeList.forEach((el) => {
        el.id = results.message;
      });
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
        this.AddGroupDetail(this._detailCodeList);
      });
  }

  AddGroupDetail(data: DefaultCodesModel[]) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Admin/AddNewCodeDetail', data
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
        this.GetAllCodeGroups();
      });
  }

  GetHpSetup() {

    this._busy = true;

    this._http.get(this._baseUrl + 'api/Admin/GetHpSetup'
    ).subscribe((results: HpSetup[]) => {
      this.hpsetupList = [];
      this.hpsetupList = results;
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => { this._busy = true; })

  }


  UpdateHpSetup(data: HpSetup) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Admin/UpdateHpSetup', data
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
      this._busy = true;
        this.GetHpSetup();
      })

  }

  DeleteHpSetup(data: HpSetup) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Admin/DeleteHpSetup', data
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = true;
        this.GetHpSetup();
      })

  }

  NewHpSetup(data: HpSetup) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Admin/NewHpSetup', data
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
      this._busy = true;
        this.GetHpSetup();
      })

  }

}
