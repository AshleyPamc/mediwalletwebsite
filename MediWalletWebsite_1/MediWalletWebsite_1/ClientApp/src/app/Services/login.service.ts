import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Route, Router } from '@angular/router';
import { LoginDetailsModel } from '../Models/LoginDetailsModel';
import { ValidationResultsModel } from '../Models/ValidationResultsModel';
import { DiagCodes } from '../Models/diag-codes';
import { Emails } from '../Models/emails';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _loginDetails: LoginDetailsModel = new LoginDetailsModel();
  private _error: boolean = false;
  private _busy: boolean = false;
  private _incorrect: boolean = false;
  private _isLoggedIn: boolean = false;
  private _diagList: DiagCodes[] = [];
  private _emailList: Emails[] = [];







  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string, private _router: Router) { }

  public get loginDetails(): LoginDetailsModel {
    return this._loginDetails;
  }
  public set loginDetails(value: LoginDetailsModel) {
    this._loginDetails = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get incorrect(): boolean {
    return this._incorrect;
  }
  public set incorrect(value: boolean) {
    this._incorrect = value;
  }
  public get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }
  public set isLoggedIn(value: boolean) {
    this._isLoggedIn = value;
  }
  public get diagList(): DiagCodes[] {
    return this._diagList;
  }
  public set diagList(value: DiagCodes[]) {
    this._diagList = value;
  }
  public get emailList(): Emails[] {
    return this._emailList;
  }
  public set emailList(value: Emails[]) {
    this._emailList = value;
  }

  ConfirmLogin() {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Login/ComfirmLogin', this._loginDetails

    ).subscribe((results: LoginDetailsModel) => {
      if (results.success) {
        this._loginDetails = results;
        this._isLoggedIn = true;
        this._router.navigateByUrl('/Dash');
      } else {
        this.incorrect = true;
        this._isLoggedIn = false;
      }
    },
      (error) => {
        console.log(error);
        this.busy = false;
        this.error = true;
      },
      () => {
        this.busy = false;
        this.GetSettingDiags();
      });
  }

  UpdatePassword() {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Login/UpdateLogin', this._loginDetails

    ).subscribe((results: LoginDetailsModel) => {
      this.loginDetails = results;
    },
      (error) => {
        console.log(error);
        this.busy = false;
        this.error = true;
      },
      () => {
        this.busy = false;
      });
  }

  GetSettingDiags() {
    this._busy = true;

    this._http.get(this._baseUrl + 'api/Transaction/GetSettingDiags'
    ).subscribe((results: DiagCodes[]) => {
      this.diagList = [];
      this.diagList = results;
    },
      (error) => { console.log(error) },

      () => {
        this._busy = false;
        this.GetEmails();
      })
  }

  GetEmails() {
    this._busy = true;

    this._http.post(this._baseUrl + 'api/Login/GetLinkedEmails',this.loginDetails
    ).subscribe((results: Emails[]) => {
      this.emailList = [];
      this.emailList = results;
    },
      (error) => { console.log(error) },

      () => {
        this._busy = false;
      })
  }
}
