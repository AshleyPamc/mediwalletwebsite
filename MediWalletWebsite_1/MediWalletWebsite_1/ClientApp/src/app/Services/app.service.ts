import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ValidationResultsModel } from '../Models/ValidationResultsModel';
import { TransactionService } from './transaction.service';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private _busy: boolean = false;
  private _error: boolean = false;
  private _webservice: boolean = false;



  constructor(public _router: Router, private _loginService: LoginService, private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string
    , private _transactionService: TransactionService, private _adminService: AdminService) { }

  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get webservice(): boolean {
    return this._webservice;
  }
  public set webservice(value: boolean) {
    this._webservice = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }

  public get LoginService(): LoginService {
    return this._loginService;
  }

  public get TransactionService(): TransactionService {
    return this._transactionService;
  }

  public get AdminService(): AdminService {
    return this._adminService;
  }


  TestConnection() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Login/TestConnection', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache",
        "accept":"application/json"

      })
    }
    ).subscribe((results: ValidationResultsModel) => {
      if (results.valid) {
        this.webservice = false;
        this._router.navigateByUrl('/Login');
      } else {
        this.webservice = true;
      }
    },
      (error) => {
        console.log(error);
        this._busy = false;
        this.error = true;
      },

      () => {
        this._busy = false;
      });
  }
}
