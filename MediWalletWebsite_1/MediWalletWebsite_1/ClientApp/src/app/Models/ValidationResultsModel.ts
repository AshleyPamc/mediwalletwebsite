export class ValidationResultsModel {
  valid: boolean;
  message: string;
  extra: string;
}
