export class LoginDetailsModel{
    username: string;
    password: string;
    userType: number;
    createBy :string;
    createDate:Date;
    changeBy:string;
    changeDate:Date;
    success: boolean;
    errorMessage:string;
    provID:string;
  bureauId: string;
  brokerId: string;
    specCode:string
    description:string
    firstname:string
    lastname:string
    contact:string
    contract:string
    lobcode:string
  lobdesc: [] = [];
  client: string;
  memberIdNumber: string;
  memberNumber: string;
  newPassword: string;
  uuid: string;
  dentist: boolean;
  gp: boolean;

}
