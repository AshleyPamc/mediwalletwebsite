export class DefaultCodesModel {
  public heading: string;
  public user: string;
  public id: string;
  public enabled: boolean;
  public added: boolean;
  public code: string;
  public phcode: string;
  public enterQty: boolean;
  public defaultQty: number;
  public enterTooth: boolean;
  public dependantOnCode: boolean;
  public depCode: string;
  public desc: string;
  public page: number;
  public price: string;
  public provid: string;
}
