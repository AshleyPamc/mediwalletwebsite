import { Component, Input, OnInit } from '@angular/core';
import { AppService } from './Services/app.service';
import { FormGroup, FormBuilder, Validators, Validator, ValidationErrors } from '@angular/forms';
import { History } from './Models/history';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{
  title = 'app';

  private _openPassword: boolean = false;
  private _errorOld: string;
  private _errorNew: string;
  private _errorConfirm: string;
  private _errorMs: ValidationErrors;

  @Input() passwordForm: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder, private _router: Router) {
    this._appService.TestConnection();
    this._errorMs = {
      required: 'This field is required!!',
      invalid: 'This field is invalid!!',
      match:'Passwords does not match!!'
    }
  }

  /*getters and setters*/
  public get errorNew(): string {
    return this._errorNew;
  }
  public set errorNew(value: string) {
    this._errorNew = value;
  }
  public get errorOld(): string {
    return this._errorOld;
  }
  public set errorOld(value: string) {
    this._errorOld = value;
  }
  public get openPassword(): boolean {
    return this._openPassword;
  }
  public set openPassword(value: boolean) {
    this._openPassword = value;
  }
  public get errorConfirm(): string {
    return this._errorConfirm;
  }
  public set errorConfirm(value: string) {
    this._errorConfirm = value;
  }



  ngOnInit() {
    this.passwordForm = this.fb.group({
      'old': ['', Validators.required],
      'new': ['', Validators.required],
      'confirm': ['', Validators.required]
    });
   
  }


  /*Open ChangePassword Modal*/ 
  ChangePassword() {
    this.passwordForm.reset();
    this.openPassword = true;
  }


  /*Validation for old password*/
  ValidateOldPassword() {
    let old:string = this.passwordForm.get('old').value;
    if ((old == '') || (old == null) || (old == undefined)) {
      this._errorOld = this._errorMs['required'];
      this.passwordForm.controls['old'].setErrors(this._errorMs['required']);

    }

    if (old !== this._appService.LoginService.loginDetails.password) {
      this._errorOld = this._errorMs['invalid'];
      this.passwordForm.controls['old'].setErrors(this._errorMs['invalid']);
    } else {
      this._errorOld = "";
      this.passwordForm.controls['old'].setErrors(null);
    }


  }

  /*Confirm passwords match*/
  ValidateNewPassword() {
    let c: string = this.passwordForm.get('new').value;
    let d: string = this.passwordForm.get('confirm').value;

    if ((c == '') || (c == null) || (c == undefined)) {
      this._errorNew = this._errorMs['required'];
      this.passwordForm.controls['new'].setErrors(this._errorMs['required']);

    }

    if ((d == '') || (d == null) || (d == undefined)) {
      this._errorConfirm = this._errorMs['required'];
      this.passwordForm.controls['confirm'].setErrors(this._errorMs['required']);

    }

    if (d !== c) {
      this._errorConfirm = this._errorMs['match'];
      this._errorNew = this._errorMs['match'];
      this.passwordForm.controls['new'].setErrors(this._errorMs['match']);
      this.passwordForm.controls['confirm'].setErrors(this._errorMs['match']);
    } else {
      this._errorConfirm = "";
      this._errorNew = "";
      this.passwordForm.controls['new'].setErrors(null);
      this.passwordForm.controls['confirm'].setErrors(null);
    }
  }

  /*update password*/
  UpdatePassword() {
    let c: string = this.passwordForm.get('new').value;
    this._appService.LoginService.loginDetails.newPassword = c;

    this._appService.LoginService.UpdatePassword();
    this.openPassword = false;
    this.passwordForm.reset();
  }

  /*Start capturing claim when clicking on a balance in the sidenav*/
  StartClaimSubmission(data: History) {
    this._appService.TransactionService.cardnumber = data.memb;
    this._router.navigateByUrl('/Capture-Codes');
    this._appService.TransactionService.cardnumber = data.memb;
    this._appService.TransactionService.amount = data.amount;
    this._appService.TransactionService.codeList = [];
    this._appService.TransactionService.BalanceCheck(false);
    
  }

  /*Clear all details for claim capture screen*/
  ClearDetails() {
    if (this._appService.TransactionService.balanceSuccess) {

    } else {
      this._appService.TransactionService.amount = "";
      this._appService.TransactionService.cardnumber = "";
      this._appService.TransactionService.memberDetails = "";
    }
  }

}
