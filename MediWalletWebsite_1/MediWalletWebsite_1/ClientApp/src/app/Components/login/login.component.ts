import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../Services/app.service';
import { LoginDetailsModel } from '../../Models/LoginDetailsModel';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() loginForm: FormGroup;


  constructor(private fb: FormBuilder, public _appService: AppService) {

  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    });



  }

  Login() {
    this._appService.LoginService.loginDetails.username = this.loginForm.get('username').value;
    this._appService.LoginService.loginDetails.password = this.loginForm.get('password').value;

    this._appService.LoginService.ConfirmLogin();

  }


}
