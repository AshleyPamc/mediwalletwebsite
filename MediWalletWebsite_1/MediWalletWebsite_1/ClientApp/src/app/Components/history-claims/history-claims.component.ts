import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Services/app.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-history-claims',
  templateUrl: './history-claims.component.html',
  styleUrls: ['./history-claims.component.css']
})
export class HistoryCLaimsComponent implements OnInit {

  @Input() SearchForm: FormGroup;

  private count: number = 0;

  constructor(public _appService: AppService, private fb: FormBuilder) {
    //this._appService.TransactionService.GetAllClaims(this._appService.LoginService.loginDetails);
  }

  ngOnInit() {
    this.SearchForm = this.fb.group({
      'search': []
    });
  }

  Search() {
    let c : string = this.SearchForm.get('search').value;

    if (c != null && c != undefined && c != '') {
      this._appService.TransactionService.historyClaims =
        this._appService.TransactionService.OrgHistoryClaims.filter(a => {

          if ((a.mediRef.toUpperCase().includes(c.toUpperCase())) || (a.membId.toUpperCase().includes(c.toUpperCase()))
            || (a.qty.includes(c)) || (a.drcRef.toUpperCase().includes(c.toUpperCase())) ) {
            return a;
          }
        });

    } else {
      this._appService.TransactionService.historyClaims =
        this._appService.TransactionService.OrgHistoryClaims;
      this.SearchForm.reset();
    }
  }

  RemoveSearch() {
    this._appService.TransactionService.historyClaims =
      this._appService.TransactionService.OrgHistoryClaims;

    this.SearchForm.reset();
  }

  SortByDate() {
    
    if (this.count == 0) {
      this.count++;
      this._appService.TransactionService.historyClaims = this._appService.TransactionService.historyClaims.sort(
        (a, b) => a.qty > b.qty ? -1 : 1);
    } else {
      this.count = 0;
      this._appService.TransactionService.historyClaims = this._appService.TransactionService.historyClaims.sort(
        (a, b) => a.qty > b.qty ? 1 : -1);
      //this.count++;
    }
  }
}
