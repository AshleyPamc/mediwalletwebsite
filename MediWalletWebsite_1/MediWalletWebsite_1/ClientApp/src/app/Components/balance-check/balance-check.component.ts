import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { AppService } from '../../Services/app.service';

@Component({
  selector: 'app-balance-check',
  templateUrl: './balance-check.component.html',
  styleUrls: ['./balance-check.component.css']
})
export class BalanceCheckComponent implements OnInit {

  @Input() OTPForm: FormGroup;
  private _errMsg: ValidationErrors;
  private _membError: string;
  private _numError: string;


  constructor(private fb: FormBuilder, public _appService: AppService) {
    this._errMsg = {
      'required': 'This field is required!!',
      'invalid':'This field is incorrect!!'
    }
    
  }

  ngOnInit() {
    this.OTPForm = this.fb.group({
      'membId': ['', Validators.required],
      'amount': ['', Validators.required]
    })
  }

  /*Getters And Setters*/

  public get membError(): string {
    return this._membError;
  }
  public set membError(value: string) {
    this._membError = value;
  }
  public get numError(): string {
    return this._numError;
  }
  public set numError(value: string) {
    this._numError = value;
  }


  /*Clear all fields*/
  Clear() {
    this.OTPForm.reset();
    this._membError = "";
    this._numError = "";
  }

  /*Process balance check*/
  BalaceCheck() {
    let c: string = this.OTPForm.get('membId').value;

    if ((c == "") || (c == undefined) || (c == null)) {
      this.membError = this._errMsg['required'];
      this.OTPForm.controls['membId'].setErrors(this._errMsg['required']);
    } else {
      this._appService.TransactionService.memberDetails = c;
      this._appService.TransactionService.cardnumber = c;
      this._appService.TransactionService.amount = this.OTPForm.get('amount').value.toString();
      this._appService.TransactionService.BalanceCheck(true);
    }
  }
}
