import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { AppService } from '../../Services/app.service';
import { ClaimCodesModel } from '../../Models/ClaimCodesModel';
import { formatDate } from '@angular/common';
import { DefaultCodesModel } from '../../Models/DefaultCodesModel';
import { JoyrideService } from 'ngx-joyride';
import { Emails } from '../../Models/emails';
import { Router } from '@angular/router';

@Component({
  selector: 'app-capture-codes',
  templateUrl: './capture-codes.component.html',
  styleUrls: ['./capture-codes.component.css']
})
export class CaptureCodesComponent implements OnInit {

  @Input() DiagForm: FormGroup;
  @Input() ClaimForm: FormGroup;
  @Input() PinForm: FormGroup;
  @Input() DefaultCodeForm: FormGroup;
  @Input() Email: FormGroup;

  public procError: string;
  public diagError: string;
  public qtyError: string;
  public priceError: string;
  public openTansaction: boolean = false;
  public openEditCodeModal: boolean = false;
  public selectedEditCode: ClaimCodesModel = new ClaimCodesModel();
  public openOtpTansaction: boolean = false;
  public OtpError: string;
  public alertBalance: boolean = true;
  public popupBalance: boolean = false;
  public _date: string = formatDate(new Date, "yyyy/MM/dd", 'en-za', '+0200');
  private _errMsg: ValidationErrors;
  public proceed: boolean = false;
  public showAddCodeForm: boolean = false;
  public openNewModel: boolean = false;
  public diagCorrect: boolean = false;
  public linesCorrect: boolean = false;
  public remaining: number = 0.00;
  public memberLiable: string;
  public providerLiable: string;
  public confirming: boolean = false;
  public total: string = ""
  public showInputEmail: boolean = false;

  constructor(private fbe: FormBuilder,private fbOtb: FormBuilder, private fb: FormBuilder, public _appService: AppService, private dfFb: FormBuilder, private fbDiag: FormBuilder, private readonly joyrideService: JoyrideService) {
    this._errMsg = {
      'required': 'This field is required!!',
      'invalid':'This field is invalid!!',
    }
    let c = this._appService.TransactionService.cardnumber;
    if ((c == undefined) || (c == null) || (c == "")) {
      this.alertBalance = false;
      this.popupBalance = true;
    }

    this._appService.TransactionService.GetDefaultCodeGroups();
    this.GetTotal();
  }

  ngOnInit() {
    this.ClaimForm = this.fb.group({
      'proc': ['', Validators.required],
      'qty': ['', Validators.required],
      'price': ['', Validators.required],
      'diag': ['', Validators.required],
      'diagList': ['', Validators.required],
      'tooth': []
    });
    this.PinForm = this.fbOtb.group({
      'otp': ['', Validators.required],
    });
    this.DefaultCodeForm = this.dfFb.group({
      'tooth': [],
      'qty': ['', Validators.required],
      'unit': ['', Validators.required],
      'diag': ['', Validators.required],
      'diagList': ['', Validators.required],
    });
    this.DiagForm = this.fbDiag.group({
      'diag': [],
      'diagList': ['', Validators.required],
    });
    this.Email = this.fbe.group({
      'mails': [],
      'address': []
    });

    this.GetTotal();

  }

  onClick() {
    //this.joyrideService.startTour({
    //  steps: ['Capture_1'],
    //  themeColor: '#313131'
    //})
  }

  /*validate procedure code*/
  ValidateProc() {
    let c = this.ClaimForm.get('proc').value;

    if ((c == "") || (c == undefined) || (c == null)) {
      this.procError = this._errMsg['required'];
      this.ClaimForm.controls['proc'].setErrors(this._errMsg['required']);
    } else {
      this.procError = "";
      this.ClaimForm.controls['proc'].setErrors(null);
      let p: ClaimCodesModel = new ClaimCodesModel();
      p.code = c;
      p.provid = this._appService.LoginService.loginDetails.provID;
      this._appService.TransactionService.ValidateProcCode(p);
      setTimeout((a) => {
        if (this._appService.TransactionService.procDesc.valid) {
          this.procError = this._appService.TransactionService.procDesc.message
          this.ClaimForm.get('price').setValue(this._appService.TransactionService.procDesc.extra)
        } else {
          this.procError = this._errMsg['invalid'];
          this.ClaimForm.controls['proc'].setErrors(this._errMsg['invalid']);
        }
      }, 100);

      
    }
  }

  /*validate qty*/
  ValidateQty() {
    let c = this.ClaimForm.get('qty').value;

    if ((c == "") || (c == undefined) || (c == null)) {
      this.qtyError = this._errMsg['required'];
      this.ClaimForm.controls['qty'].setErrors(this._errMsg['required']);
    } else {
      this.qtyError = "";
      this.ClaimForm.controls['qty'].setErrors(null);
    }
  }

  /*validate amount*/
  ValidatePrice() {
    let c = this.ClaimForm.get('price').value;

    if ((c == "") || (c == undefined) || (c == null)) {
      this.priceError = this._errMsg['required'];
      this.ClaimForm.controls['price'].setErrors(this._errMsg['required']);
    } else {
      this.priceError = "";
      this.ClaimForm.controls['price'].setErrors(null);
    }
  }

  /*add claim to grid*/
  AddCode() {
    let c: ClaimCodesModel = new ClaimCodesModel();

    c.code = this.ClaimForm.get("proc").value;
    c.desc = this._appService.TransactionService.procDesc.message;
    c.qty = this.ClaimForm.get("qty").value.toString();
    c.singleUnitAmount = this.ClaimForm.get("price").value;
    c.diag = this.ClaimForm.get('diag').value;
    c.total = ((+c.qty) * (+c.singleUnitAmount)).toFixed(2);
    c.provid = this._appService.LoginService.loginDetails.provID;
    c.membId = this._appService.TransactionService.cardnumber;
    c.toothNo = this.ClaimForm.get('tooth').value; 

    let total: number = (+c.total) + (+this.GetTotal());

    

      this._appService.TransactionService.codeList.push(c);

      if (this._appService.TransactionService.codeList.length < 2) {
        this._appService.TransactionService.diagCode = c.diag;
      }

      this.ClaimForm.reset();

      this.procError = "";
      this.qtyError = "";
      this.priceError = "";
    
    this.showAddCodeForm = false;
    this.openNewModel = false;

    if (this._appService.TransactionService.codeList.length == 1) {
      this.onClick();
    }
    //console.log(!(this._appService.TransactionService.codeList.length > 0) || !(this.DiagForm.valid));
    //console.log(!(this._appService.TransactionService.codeList.length > 0));
    //console.log(!(this.DiagForm.valid));
    //this.linesCorrect = true;
  }

  /*Populates the table with codes that was defaultly setup*/
  AddDefaultCode(data: DefaultCodesModel) {
    data.provid = this._appService.LoginService.loginDetails.provID;
    this._appService.TransactionService.GetDefaultCodeDetails(data);
    this._appService.TransactionService.busy = true;
    setTimeout(() => {
      this.GetTotal();
      this.DefaultCodeForm.reset();
      this._appService.TransactionService.busy = false;
      if (this._appService.TransactionService.codeList.length > 0) {
        this.onClick();
      }
      let total = +this.GetTotal();


    }, 150);
    if (this._appService.TransactionService.codeList.length >= 0) {
      this.onClick();
    }

    //console.log(!(this.diagCorrect) && !(this.linesCorrect))
    //console.log(!(this.diagCorrect))
    //console.log(!(this.linesCorrect));
    //this.linesCorrect = true;
  }

  /*Clear all fields*/
  Clear() {
    this.ClaimForm.reset();
    this.DefaultCodeForm.reset();
    this.DiagForm.reset();

    this.procError = "";
    this.priceError = "";
    this.qtyError = "";
    this.diagError = "";
    this.showAddCodeForm = false;
  }

  /*Delete code from grid*/
  RemoveCode(c: ClaimCodesModel) {
   
    let count: number = 0;
    let index: number = 0;
    this._appService.TransactionService.codeList.forEach((el) => {
      if ((el.code == c.code) && (el.qty == c.qty) && (el.singleUnitAmount == c.singleUnitAmount) && (el.diag == c.diag)) {
        index = count;
      }
      count++;
    });

    this._appService.TransactionService.codeList.splice(index, 1);
    let cd = this.GetTotal();
  }

  /*validate diagnosis code*/
  ValidateDiag() {
    //console.log(!(this.diagCorrect) && !(this.linesCorrect))
    //console.log(!(this.diagCorrect))
    //console.log(!(this.linesCorrect));

    let c: string = this.DiagForm.get('diag').value;
    if ((c == "") || (c == undefined) || (c == null)) {
      this.diagCorrect = true;
    } else {
      this.diagError = "";
      this.DiagForm.controls['diag'].setErrors(null);
      let p: ClaimCodesModel = new ClaimCodesModel();
      p.diag = c;
      this._appService.TransactionService.ValidateDiagCode(p);
      setTimeout((a) => {
        if (this._appService.TransactionService.diagdesc.valid) {
          this._appService.TransactionService.diagCodeFinale = this.DiagForm.get('diag').value;
          this.diagError = this._appService.TransactionService.diagdesc.message;
          this.diagCorrect = true;
        } else {
          this.diagError = this._errMsg['invalid'];
          this.DiagForm.controls['diag'].setErrors(this._errMsg['invalid']);
          this.diagCorrect = false;
        }
      }, 200)
    }

  }

  SetDiag() {
    let c = this.DiagForm.get('diagList').value;
    this.diagCorrect = true;
    if (c !== "OTHER") {
      this._appService.TransactionService.codeList.forEach((el) => {
        el.diag = c.toUpperCase();
      });
      this.diagCorrect = true;
    }

    this.DiagForm.get('diagList').markAsTouched();
    this.DiagForm.get('diag').markAsTouched();
    //console.log(!(this.diagCorrect) && !(this.linesCorrect))
    //console.log(!(this.diagCorrect))
    //console.log(!(this.linesCorrect));

  }

  /*Get total claim amount*/
  GetTotal(): string {
    let count: number = 0;
    let code: number = 0;
    this.remaining = +this._appService.TransactionService.amount;
    this._appService.TransactionService.codeList.forEach((el) => {
      code++;
      if ((el.total == '') || (el.total == null) || (el.total == undefined) ){
        el.total = '0.00';
      }

      
      if (code == 1) {
        

        ////console.log("code if " + this.remaining.toFixed(2));
        this.remaining = this.remaining - +el.total;
        if (el.total !== "0.00") {
          if (this.remaining == 0) {
            el.provpay = el.total;
          } else if (this.remaining < 0) {
            let c = 0 - this.remaining;
            el.provpay = (+el.total - c).toFixed(2);
            el.membpay = (c).toFixed(2);
          } else if (this.remaining > 0) {
            el.provpay = el.total;
           // el.provpay = "";
          }
        }
      } else {
        if (el.total !== "0.00") {
          this.remaining = this.remaining - +el.total;
          if (this.remaining > +el.total) {
            // this.remaining = this.remaining - +el.total;
            el.membpay = "0.00";
            el.provpay = el.total;
          } else if (this.remaining < +el.total && this.remaining > 0) {
            el.provpay = el.total;
           // let c = 0 - (this.remaining - +el.total);
            el.membpay = "0.00";
            //this.remaining = this.remaining - +el.total;
          } else if (this.remaining <= 0) {
           // el.membpay = el.total;
            let c = 0 - this.remaining;
            if (c >= +el.total) {
              el.membpay = el.total
            } else if (c < +el.total) {
              el.provpay = (+el.total - c).toFixed(2);
              el.membpay = (c).toFixed(2);
            }
           // this.remaining = this.remaining - +el.total;
          }
        }

      //  //console.log("code else " + this.remaining.toFixed(2));
        
      }


     

      count = count + (+el.total);
    });

    this.memberLiable = "0.00";
    this.providerLiable = "0.00";

    this._appService.TransactionService.codeList.forEach((el) => {
      this.memberLiable = (+this.memberLiable + +el.membpay).toFixed(2);
      this.providerLiable = (+this.providerLiable + +el.provpay).toFixed(2);
    });
    //console.log(this._appService.TransactionService.codeList)
    return count.toFixed(2);
  }

  /*Claim submission confirmation*/
  SubmitClaim() {
    this.openTansaction = true;
    if (this.DiagForm.get('diagList').value == "OTHER") {
      let c: string = this.DiagForm.get('diag').value;
      if (c !== null && c !== undefined) {
        this._appService.TransactionService.codeList.forEach((el) => {
          el.diag = c.toUpperCase();
        })
      }

    } else {
      let c: string = this.DiagForm.get('diagList').value;
      this._appService.TransactionService.codeList.forEach((el) => {
        el.diag = c.toUpperCase();
      })
    }

  }

  /*Send OTP*/
  SendOTP() {
    this._appService.TransactionService.SendOtp();
  }

  /*Confirm OTP*/
  StartTransaction() {
    this.openTansaction = false;
    this.openOtpTansaction = true;
    let c: string = this.DiagForm.get('diagList').value;
    if (c !== "OTHER") {
      this._appService.TransactionService.codeList.forEach((el) => {
        el.diag = c.toUpperCase();
      })
    } else {
      let d: string = this.DiagForm.get('diag').value;
      this._appService.TransactionService.codeList.forEach((el) => {
        el.diag = d.toUpperCase();
      })
    }

    this.SendOTP();
  }

  /*Submit claim*/
  ProcessTransaction() {
    let amount = this.GetTotal();
    this._appService.TransactionService.totalMedi = this.providerLiable;
    this._appService.TransactionService.ProcessPayment(this.providerLiable);
    
    this.PinForm.reset();
    this.OtpError = "";
    this.openOtpTansaction = false;

    this.Clear();

  }

  /*Confirm OTP*/
  ConfirmOtp() {
    let c: string = this.PinForm.get('otp').value;
    if ((c == undefined) || (c == "") || (c == null)) {
      this.PinForm.controls['otp'].setErrors(this._errMsg['required']);
      this.OtpError = this._errMsg['required'];
    } else {
      this.PinForm.controls['otp'].setErrors(null);
      this.OtpError = "";
      this._appService.TransactionService.ConfirmOtp(c);
      this._appService.TransactionService.busy = true;
      this.confirming = true;
      setTimeout((a) => {
        this._appService.TransactionService.busy = true;
        if (this._appService.TransactionService.otpSend.valid) {
          this.OtpError = "";
          this.PinForm.controls['otp'].setErrors(null);
          //console.log("start Transaction");
          this.openOtpTansaction = false;
          this.confirming = false;
          this.ProcessTransaction();

        } else {
          this.OtpError = this._errMsg['invalid'];
          this.PinForm.controls['otp'].setErrors(this._errMsg['invalid']);
          this.confirming = false;
        }
      //  this._appService.TransactionService.busy = false;
      }, 5000);




    }

  }

  /*Add new Detail Line*/
  AddLine() {
    this.ClaimForm.reset();
    this.diagError = "";
    this.procError = "";
    this.qtyError = "";
    this.priceError = "";
    //this.showAddCodeForm = true;
    if (this._appService.TransactionService.codeList.length >= 1) {
      this.ClaimForm.controls['diag'].setValue(this._appService.TransactionService.diagCode);
      this.ClaimForm.controls['diag'].disable();
    } else {
      this.ClaimForm.controls['diag'].enable();
    }

    this.openNewModel = true;
    
  }

  /* Code that fires when the units are entered */
  UnitBlur(code: ClaimCodesModel) {
    let c: string = this.DefaultCodeForm.get('unit').value;
    this._appService.TransactionService.codeList.forEach((el) => {
      if (el.code == code.code) {
        el.singleUnitAmount = c;
        el.total = ((+el.qty) * (+el.singleUnitAmount)).toFixed(2);
      }
    });

    let total = this.GetTotal();

    this.DefaultCodeForm.get('unit').reset();
  }

  /* Code that fires when qty is entered*/
  QtyBlur(code: ClaimCodesModel) {
    let c: string = this.DefaultCodeForm.get('qty').value;
    this._appService.TransactionService.codeList.forEach((el) => {
      if (el.code == code.code) {
        el.qty = c;

        if (el.singleUnitAmount !== "" && el.singleUnitAmount !== null && el.singleUnitAmount !== undefined) {
          el.total = ((+el.qty) * (+el.singleUnitAmount)).toFixed(2);
        }
      }
    });

    let total = this.GetTotal();

    //this.DefaultCodeForm.get('qty').reset();
  }

  /* Code that fires when thooth numbers are entered*/
  ToothBlur(code: string) {
    
    let c: string = this.DefaultCodeForm.get('tooth').value;
    this._appService.TransactionService.codeList.forEach((el) => {
      if (el.code == code) {
        el.toothNo = c;

        let d: string[] = el.toothNo.split(',');

        el.qty = d.length.toString();
        if (this.openEditCodeModal) {
          this.ClaimForm.controls['qty'].setValue(el.qty);
        }

        if (el.singleUnitAmount !== "" && el.singleUnitAmount !== null && el.singleUnitAmount !== undefined) {
          el.total = ((+el.qty) * (+el.singleUnitAmount)).toFixed(2);
        }
      }
    });

    let f: ClaimCodesModel = new ClaimCodesModel();
    f.toothNo = c;

    this._appService.TransactionService.ValidateToothNo(f);
    let total = +this.GetTotal();

  }

  ToothBlurOwnCode(code: string) {

    let c: string = this.ClaimForm.get('tooth').value;
    if (c !== undefined && c!== null && c !== '') {
        this._appService.TransactionService.codeList.forEach((el) => {
      if (el.code == code) {
        el.toothNo = c;

        let d: string[] = el.toothNo.split(',');

        el.qty = d.length.toString();
        if (this.openEditCodeModal) {
          this.ClaimForm.controls['qty'].setValue(el.qty);
        }

        if (el.singleUnitAmount !== "" && el.singleUnitAmount !== null && el.singleUnitAmount !== undefined) {
          el.total = ((+el.qty) * (+el.singleUnitAmount)).toFixed(2);
        }
      }
    });

      let f: ClaimCodesModel = new ClaimCodesModel();
      f.toothNo = c;

      this._appService.TransactionService.ValidateToothNo(f);
      let total = +this.GetTotal();


    }
  }

  ToothBlurEdit(code: string) {
    let c: string = this.ClaimForm.get('tooth').value;
    if (c !== undefined && c !== null && c !== '') {
      this._appService.TransactionService.codeList.forEach((el) => {
        if (el.code == code) {
          el.toothNo = c;

          let d: string[] = el.toothNo.split(',');

          el.qty = d.length.toString();
          if (this.openEditCodeModal) {
            this.ClaimForm.controls['qty'].setValue(el.qty);
          }

          if (el.singleUnitAmount !== "" && el.singleUnitAmount !== null && el.singleUnitAmount !== undefined) {
            el.total = ((+el.qty) * (+el.singleUnitAmount)).toFixed(2);
          }
        }
      });

      let f: ClaimCodesModel = new ClaimCodesModel();
      f.toothNo = c;

      this._appService.TransactionService.ValidateToothNo(f);
    }
  }

  /* Code that fires when diag code is entered*/
  DiagCodeBlur(code: ClaimCodesModel) {
    let c: string = this.DefaultCodeForm.get('diag').value;
    this._appService.TransactionService.codeList.forEach((el) => {
      if (el.code == code.code) {
        el.diag = c;
      }
    });
    this.DefaultCodeForm.get('diag').reset();
  }

  /* Code used to edit a code in the tabel*/
  EditCode(data: ClaimCodesModel) {
    this.selectedEditCode = new ClaimCodesModel();
    this.ClaimForm.get('tooth').enable();
    this.selectedEditCode = data;

    this.ClaimForm.controls['proc'].setValue(data.code);
    this.ClaimForm.controls['proc'].disable();
    this.ClaimForm.controls['qty'].setValue(data.qty);
    this.ClaimForm.controls['price'].setValue(data.singleUnitAmount);
    this.ClaimForm.controls['diag'].setValue(data.diag);
    this.ClaimForm.controls['tooth'].setValue(data.toothNo);
    if (data.enterTooth == false) {
      this.ClaimForm.get('tooth').disable();
    }
    this.openEditCodeModal = true;
  }

  /* Code used to update a code that was edited */
  UpdateCode() {
    let c: ClaimCodesModel = new ClaimCodesModel();

    c.code = this.selectedEditCode.code
    c.qty = this.ClaimForm.get('qty').value;
    c.singleUnitAmount = this.ClaimForm.get('price').value;
    c.diag = this.ClaimForm.get('diag').value;
    c.toothNo = this.ClaimForm.get('tooth').value;

    

    this._appService.TransactionService.codeList.forEach((el) => {
      if (el.code == this.selectedEditCode.code) {
        el.qty = c.qty;
        el.singleUnitAmount = c.singleUnitAmount;
        el.diag = c.diag;
        el.toothNo = c.toothNo;
        if (el.enterTooth) {
          let count: string[] = el.toothNo.split(',');
          el.qty = count.length.toString();
        }
        el.total = ((+el.singleUnitAmount) * (+el.qty)).toFixed(2);
      }
    });

    let total = this.GetTotal();



    this.ClaimForm.reset();

    this.openEditCodeModal = false;;

  }


  ClearLines() {
    this._appService.TransactionService.codeList = [];
  }

  EmailChange() {
    let c: string = this.Email.get("mails").value;
    if (c.toUpperCase() == "OTHER") {
      this.showInputEmail = true;
    } else {
      this.showInputEmail = false;
    }
  }

  SendEmail() {
    let c: Emails = new Emails();

    let d: string = this.Email.get("mails").value;

    if (d != '' && d != undefined && d != null) {
      if (d.toUpperCase() == "OTHER") {
        c.email = this.Email.get("address").value;
      } else {
        c.email = d;
      }

    }

    c.mediref = this._appService.TransactionService.mediRef;
    c.drcref = this._appService.TransactionService.drcRef;
    c.membid = this._appService.TransactionService.cardnumber;
    c.provid = this._appService.LoginService.loginDetails.provID;

    this._appService.TransactionService.SendEmail(c);

  }

  CancelMail() {
    this._appService.TransactionService.showEmail = false
    let c: Router;
    c.navigateByUrl("/Dash");
  }
}
