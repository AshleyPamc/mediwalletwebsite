import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Services/app.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DefaultCodesModel } from '../../Models/DefaultCodesModel';

@Component({
  selector: 'app-proc-code-setup',
  templateUrl: './proc-code-setup.component.html',
  styleUrls: ['./proc-code-setup.component.css']
})
export class ProcCodeSetupComponent implements OnInit {

  @Input() codeForm: FormGroup;
  @Input() SearchCodeForm: FormGroup;
  @Input() CodeDetailSetupForm: FormGroup;
  @Input() GroupDescForm: FormGroup;

  public _currentPage: number = 1;
  public selectedList: DefaultCodesModel[] = [];
  public currentLIst: DefaultCodesModel[] = [];
  public selectedCode: DefaultCodesModel = new DefaultCodesModel();
  public SettingModal: boolean = false;
  public OpenCodeSetup: boolean = false;
  public _edit: boolean = false;

  constructor(public _appService: AppService, private fb: FormBuilder, private fbSearch: FormBuilder, private fbDetail: FormBuilder, private fbGroup: FormBuilder) {
    this._appService.AdminService.GetAllCodeGroups();
    this.selectedList = [];
  }

  ngOnInit() {
    this.codeForm = this.fb.group({
      'add': []
    });

    this.SearchCodeForm = this.fbSearch.group({
      'search': []
    });

    this.CodeDetailSetupForm = this.fbDetail.group({
      'qtyDefault': [],
      'qty': [],
      'toothNo': [],
      'depent': [],
      'codeDep': []
    });

    this.GroupDescForm = this.fbGroup.group({
      'name': [],
      'enable':[]

    });

    this.CodeDetailSetupForm.get('qty').disable();
    this.CodeDetailSetupForm.get('codeDep').disable();
  }


  PopulateSelected(page: number) {
    this.currentLIst = [];

    this.currentLIst = this._appService.AdminService.listProcCodes.filter((a) => {
      if (a.page == page) {
        return a;
      }
    });
  }

  DecPage() {
    this._currentPage--;
    this.PopulateSelected(this._currentPage);
  }

  IncPage() {
    this._currentPage++;
    this.PopulateSelected(this._currentPage);
  }

  AddCode(code: DefaultCodesModel) {
    let c = this.codeForm.get('add').value;

    if (c) {
      this._appService.AdminService.listProcCodes.forEach((el) => {
        if (el.code == code.code ) {
          el.added = true;
        }
      });

      this.selectedCode = code;

      this.CodeDetailSetupForm.reset();
      this.SettingModal = true;

    } else {
      let index = 0;
      let count = 0;
      this._appService.AdminService.listProcCodes.forEach((el) => {
        if (el.code == code.code ) {
          el.added = false;
          this.selectedList.forEach((er) => {
            if (er.code == code.code) {
              index = count;
            }
            count++;
            //this.selectedList.splice(index, 1);
          });

        }
      });

      this.selectedList.splice(index, 1);
    }
  }

  SearchForCode() {
    let s: string = this.SearchCodeForm.get('search').value.toUpperCase();

    if ((s == '') || (s == undefined) || (s == null)) {
      this._currentPage = 1;
      this.PopulateSelected(this._currentPage);
    } else {
      this.currentLIst = [];

      this._appService.AdminService.listProcCodes.forEach((el) => {
        if ((el.code.toUpperCase().includes(s)) || (el.desc.toUpperCase().includes(s)) ||
          (el.phcode.toUpperCase().includes(s))) {
          this.currentLIst.push(el);
        }
      });
       
     
    }
  }

  EnableQty() {
    let c = this.CodeDetailSetupForm.get('qtyDefault').value;
    if (c) {
      this.CodeDetailSetupForm.get('qty').enable();
    } else {
      this.CodeDetailSetupForm.get('qty').reset();
      this.CodeDetailSetupForm.get('qty').disable();
    }
  }

  EnableDepCode() {
    let c = this.CodeDetailSetupForm.get('depent').value;
    if (c) {
      this.CodeDetailSetupForm.get('codeDep').enable();
    } else {
      this.CodeDetailSetupForm.get('codeDep').reset();
      this.CodeDetailSetupForm.get('codeDep').disable();
    }
  }

  AddDetails() {
    this.selectedCode.enterQty = this.CodeDetailSetupForm.get('qtyDefault').value;
    if (this.selectedCode.enterQty) {
      this.selectedCode.defaultQty = this.CodeDetailSetupForm.get('qty').value;
    } else {
      this.selectedCode.enterQty = false;
      this.selectedCode.defaultQty = 0;
    }
    this.selectedCode.enterTooth = this.CodeDetailSetupForm.get('toothNo').value;
    if (this.selectedCode.enterTooth) {

    } else {
      this.selectedCode.enterTooth = false;
    }
    this.selectedCode.dependantOnCode = this.CodeDetailSetupForm.get('depent').value;
    if (this.selectedCode.dependantOnCode) {
      this.selectedCode.depCode = this.CodeDetailSetupForm.get('codeDep').value;
    } else {
      this.selectedCode.dependantOnCode = false;
      this.selectedCode.depCode = "";
    }

    this.selectedList.push(this.selectedCode);
    this.SettingModal = false;
    this.CodeDetailSetupForm.reset();
    this.CodeDetailSetupForm.get('qty').disable();
    this.CodeDetailSetupForm.get('codeDep').disable();
  }

  CancelModal() {
    this.SettingModal = false;
    this.CodeDetailSetupForm.reset();
    this.CodeDetailSetupForm.get('qty').disable();
    this.CodeDetailSetupForm.get('codeDep').disable();
    this._appService.AdminService.listProcCodes.forEach((el) => {
      if (el.code == this.selectedCode.code && el.phcode == this.selectedCode.phcode) {
        el.added = false;
      }
    });
    this.currentLIst.forEach((el) => {
        if (el.code == this.selectedCode.code && el.phcode == this.selectedCode.phcode) {
          el.added = false;
        }
    });
    this.CodeDetailSetupForm.reset();
    this.CodeDetailSetupForm.get('qty').disable();
    this.CodeDetailSetupForm.get('codeDep').disable();
  }

  Edit(group: DefaultCodesModel) {
    this._edit = true;
    this._appService.AdminService.GetGroupDetailsCode(group);
      this.selectedList = this._appService.AdminService.detailCodeList;
      this.selectedList.forEach((el) => {
        this._appService.AdminService.listProcCodes.forEach((et) => {
          if (el.code == et.code) {
            et.added = true;
          }
        });
      });

      this._currentPage = 1;

      this.PopulateSelected(this._currentPage);

      this.GroupDescForm.reset();
      this.GroupDescForm.get('name').setValue(group.heading);
      this.GroupDescForm.get('enable').setValue(group.enabled);

      this.OpenCodeSetup = true;


  }

  Delete(group: DefaultCodesModel) {

    this._appService.AdminService.DeleteGroup(group);

  }

  AddNewLine() {
    this._appService.AdminService.busy = true;
    this._edit = false;
    this.GroupDescForm.reset();
    this.selectedList = [];
    this.selectedCode = new DefaultCodesModel();
    this._currentPage = 1;
    this._appService.AdminService.GetAllProcedureCodes();
    setTimeout(() => {
      this.PopulateSelected(this._currentPage);
      this.OpenCodeSetup = true;
      this._appService.AdminService.busy = false;
    },150)
      



  }


  AddCodeGroup() {
    let c: DefaultCodesModel = new DefaultCodesModel();

    c.enabled = this.GroupDescForm.get('enable').value;
    c.heading = this.GroupDescForm.get('name').value;
    c.user = this._appService.LoginService.loginDetails.username;
    this.selectedList[0].user = c.user;
    this._appService.AdminService.detailCodeList = [];

    this._appService.AdminService.detailCodeList = this.selectedList;

    this._appService.AdminService.AddGroup(c);
    this.OpenCodeSetup = false;

    

  }
}
