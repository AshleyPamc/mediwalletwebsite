import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Services/app.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Input() smsForm: FormGroup;

  public url: string = "https://www.ftapp.co.za/Transact/SMSAppFailure/096abd39-dcb8-416a-b5cc-5850a863d191";
  public urlSafe: SafeResourceUrl;
  public openSms: boolean = false;

  constructor(public _appService: AppService, private fb: FormBuilder, public sanitizer: DomSanitizer) {
    this._appService.TransactionService.loginDetails = this._appService.LoginService.loginDetails;
  }

  ngOnInit() {
    this.smsForm = this.fb.group({
      'num': []
    });

    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    //"\"ng build --base-href \"/DRCMediWallet/\""
  }

}
