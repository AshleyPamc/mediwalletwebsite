import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Services/app.service';
import { HpSetup } from '../../Models/hp-setup';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-hpsetup',
  templateUrl: './hpsetup.component.html',
  styleUrls: ['./hpsetup.component.css']
})
export class HPSetupComponent implements OnInit {

  @Input() hpsetupForm: FormGroup;
  public openModelUpdate: boolean = false;
  public openModelNew: boolean = false;
  public selectedHp: HpSetup = new HpSetup();

  constructor(public _appService: AppService, private fb: FormBuilder) {

    this._appService.AdminService.GetHpSetup();
  }

  ngOnInit() {
    this.hpsetupForm = this.fb.group({
      'hpcode': [],
      'desc': [],
      'enabled':[]
    })
  }

  DeleteSetup(data: HpSetup) {
    data.user = this._appService.LoginService.loginDetails.username;
    this._appService.AdminService.DeleteHpSetup(data);

  }

  NewHpSetup() {

    let c: HpSetup = new HpSetup();

    c.user = this._appService.LoginService.loginDetails.username;
    c.hpcode = this.hpsetupForm.get('hpcode').value;
    c.descr = this.hpsetupForm.get('desc').value;
    let d = this.hpsetupForm.get('enabled').value;
    if (d == '1') {
      c.enabled = true;
    } else {
      c.enabled = false;
    }
   

    this._appService.AdminService.NewHpSetup(c);

    this.hpsetupForm.reset();
    this.openModelNew = false;

  }

  openHpsetupEdit(data: HpSetup) {

    this.selectedHp = new HpSetup();
    this.selectedHp = data;
    this.hpsetupForm.reset();
    this.hpsetupForm.controls['desc'].setValue(data.descr);
    this.hpsetupForm.controls['hpcode'].setValue(data.hpcode);
    if (data.enabled) {
      this.hpsetupForm.controls['enabled'].setValue('1');
    } else {
      this.hpsetupForm.controls['enabled'].setValue('0');
    }

    this.openModelUpdate = true;

  }

  openHpsetupNew() {

    this.hpsetupForm.reset();
    this.openModelNew = true;

  }

  updateHpSetup() {
    let c: HpSetup = new HpSetup();

    c.user = this._appService.LoginService.loginDetails.username;
    c.hpcode = this.hpsetupForm.get('hpcode').value;
    c.descr = this.hpsetupForm.get('desc').value;
    let d = this.hpsetupForm.get('enabled').value;
    if (d == '1') {
      c.enabled = true;
    } else {
      c.enabled = false;
    }
    c.id = this.selectedHp.id;

    this._appService.AdminService.UpdateHpSetup(c);
    this.hpsetupForm.reset();
    this.openModelUpdate = false;


    
  }


}
