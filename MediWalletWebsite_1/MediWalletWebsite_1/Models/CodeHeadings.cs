﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class CodeHeadings
    {
        public string id { get; set; }
        public string desc { get; set; }
        public string user { get; set; }
    }
}
