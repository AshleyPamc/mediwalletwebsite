﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class DefaultCodeModel
    {
        public string heading { get; set; }

        public string provid { get; set; }
        public string user { get; set; }
        public string id { get; set; }
        public bool enabled { get; set; }
        public string code { get; set; }

        public string phcode { get; set; }

        public string desc { get; set; }

        public bool enterQty { get; set; }

        public int defaultQty { get; set; }

        public bool enterTooth { get; set; }

        public bool dependantOnCode { get; set; }

        public string depCode { get; set; }

        public string price { get; set; }
    }
}
