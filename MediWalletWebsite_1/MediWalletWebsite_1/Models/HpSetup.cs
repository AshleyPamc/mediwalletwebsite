﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class HpSetup
    {
        public int id { get; set; }
        public string hpcode { get; set; }
        public bool enabled { get; set; }
        public string descr { get; set; }
        public string user { get; set; }
    }
}
