﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class DiagCodes
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}
