﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediWalletWebsite_1.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using PamcEzLinkLibrary;

namespace MediWalletWebsite_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : Base.ContextController
    {
        public TransactionController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("LogEvent")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel LogEvent([FromBody] EventLoggingModel data)
        {

            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@prov", data.provid));
                    cmd.Parameters.Add(new SqlParameter("@event", data.eventDesc));
                    cmd.Parameters.Add(new SqlParameter("@memb", data.member));
                    cmd.Parameters.Add(new SqlParameter("@uuid", data.uuid));
                    cmd.Parameters.Add(new SqlParameter("@amount", data.amount));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:dd").Replace("T", " ")));

                    cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.MediWallet_Log (provid,uuid,event,memberid,amount,createdate,createby,changedate,changeby)\n" +
                        $"VALUES (@prov,@uuid,@event,@memb,@amount,@date,@user,@date,@user)";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("ValidateProcCode")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProcCode([FromBody] ClaimCodesModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    cmd.Parameters.Add(new SqlParameter("@proc", data.code));

                    cmd.CommandText = $"SELECT DISTINCT SVCDESC FROM {DRCDatabase}.dbo.SERVICE_CODES WHERE REPLACE(SVCCODE,' ','') = @proc";





                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0)
                    {
                        int count = 0;

                        foreach (DataRow row in dt.Rows)
                        {
                            if (count == 0)
                            {
                                vr.message = vr.message + "* " + row["SVCDESC"].ToString() + "\n\r";
                            }
                            else
                            {
                                vr.message = vr.message + "*" + row["SVCDESC"].ToString() + "\n\r";
                            }

                            count++;
                        }

                        DataTable dtbl = new DataTable();
                        cmd.CommandText = $"SELECT HPCODE,feeset FROM {MediDatabase}.dbo.MEDIWALLET_HP_SETUP WHERE ENABLED = '1'";
                        dtbl.Load(cmd.ExecuteReader());

                        string hp = dtbl.Rows[0][0].ToString();
                        string feeset = dtbl.Rows[0][1].ToString();
                        feeset = $"'{feeset.Replace(",","','")}'"; 

                        dtbl = new DataTable();


                        dtbl = new DataTable();

                        cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET IN( {feeset}) AND PHCODE = 'P' AND BEGPROC = '{data.code.PadLeft(20, ' ')}' ";
                        dtbl.Load(cmd.ExecuteReader());
                        if (dtbl.Rows.Count > 0)
                        {
                            vr.extra = dtbl.Rows[0][0].ToString();
                        }
                        else
                        {
                            vr.extra = "";
                        }
;

                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }


                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("ValidateDiagCode")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateDiagCode([FromBody] ClaimCodesModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    cmd.Parameters.Add(new SqlParameter("@diag", data.diag));

                    cmd.CommandText = $"SELECT DISTINCT DIAGDESC FROM {DRCDatabase}.dbo.DIAG_CODES WHERE DIAGCODE = @diag";

                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0)
                    {
                        int count = 0;

                        foreach (DataRow row in dt.Rows)
                        {
                            if (count == 0)
                            {
                                vr.message = vr.message + "* " + row["DIAGDESC"].ToString() + "\n\r";
                            }
                            else
                            {
                                vr.message = vr.message + "*" + row["DIAGDESC"].ToString() + "\n\r";
                            }

                            count++;
                        }

                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }


                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("SaveData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SaveData([FromBody] List<ClaimCodesModel> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@memb", data[0].membId));
                    cmd.Parameters.Add(new SqlParameter("@uuid", data[0].id));
                    cmd.Parameters.Add(new SqlParameter("@prov", data[0].provid));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add(new SqlParameter("@user", data[0].user));
                    cmd.Parameters.Add(new SqlParameter("@medi", data[0].mediRef));
                    cmd.Parameters.Add(new SqlParameter("@name", data[0].name));
                    cmd.Parameters.Add(new SqlParameter("@surname", data[0].surname));

                    int day = 0;
                    int month = 0;
                    int year = 0;

                    string prefix = "19";

                    int curentYear = DateTime.Now.Year % 100;

                    year = Convert.ToInt32(data[0].membId.Substring(0, 2));
                    month = Convert.ToInt32(data[0].membId.Substring(2, 2));
                    day = Convert.ToInt32(data[0].membId.Substring(4, 2));

                    if (year < curentYear)
                    {
                        prefix = "20";
                    }

                    string dob = $"{prefix}{year}/{month}/{day}";

                    cmd.Parameters.Add(new SqlParameter("@dob", dob));



                    double total = 0.00;

                    foreach (var item in data)
                    {
                        total = total + Convert.ToDouble(item.total);
                    }

                    cmd.Parameters.Add(new SqlParameter("@total", total));

                    cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.Claim_Header (uuid,mediRef,provid,memberId,firstname,lastname,dateofbirth," +
                        $"net,createDate,createBy,changeDate,changeBy) \n " +
                        $"OUTPUT INSERTED.id\n" +
                        $"VALUES (@uuid,@medi,@prov,@memb,@name,@surname,@dob,@total,@date,@user,@date,@user)";

                    int id = (int)cmd.ExecuteScalar();

                    cmd.CommandText = $"UPDATE {MediDatabase}.dbo.Claim_Header SET ref = '#DRC{id}' WHERE id = '{id}'";
                    cmd.ExecuteNonQuery();

                    foreach (ClaimCodesModel code in data)
                    {

                        cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.Claim_Detail (headId,DiagCode,ProcCode,ToothNo,qty,singleUnitValue,total,PayableByProv,PayableByMemb,createDate,createBy,changeDate,changeBy) \n " +
                            $"VALUES ('{id}','{code.diag}','{code.code}','{code.toothNo}','{code.qty}','{code.singleUnitAmount}','{code.total}','{code.provpay}','{code.membpay}',@date,@user,@date,@user)";
                        cmd.ExecuteNonQuery();
                    }

                    vr.message = $"#DRC{id}";
                    SaveDataToDAT(id.ToString());

                }

            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
           
            return vr;
        }
        
        [HttpPost]
        [Route("SendEmail")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SendEmail([FromBody] Emails data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {

                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {

                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    DataTable dt = new DataTable();

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (data.email != null && data.email != "" && data.email.Length > 1 && data.email.Contains('@'))
                    {
                        cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.EmailListPerProvider  WHERE email = '{data.email}'";
                        cmd.ExecuteNonQuery();
                        dt.Load(cmd.ExecuteReader());
                        if (dt.Rows.Count == 0)
                        {
                            cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.EmailListPerProvider " +
    $"(provid,email,createdate,createby,changedate,changeby) VALUES " +
    $"('{data.provid}','{data.email}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','99'," +
    $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','99')";
                            cmd.ExecuteNonQuery();
                        }

                    }
                        

                    cmd.CommandText = cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";

                    dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    string server = "";
                    string username = "";
                    string password = "";
                    string from = "";
                    string port = "";
                    string subject = "";
                    string body = "";

                    subject = $"MEDIWALLET CLAIM FOR MEMBER {data.membid}";

                    body = $"Good day, \n\r" +
                        $"Please find below your refference number for you mediwallet claim, \n" +
                        $"submitted for member with id {data.membid} : \n\r" +
                        $"MEDIWALLET REF: {data.mediref} \n" +
                        $"DRC REF: {data.drcref}\n\r" +
                        $"Regards";

                    foreach (DataRow row in dt.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        username = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();
                    }

                    cmd.CommandText = $"SELECT EMAIL FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = '{data.provid}'";
                    dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    string email = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        email = row["EMAIL"].ToString();
                    }

                    

                    if (email != null && email != "")
                    {
                        if(data.email != null && data.email != "" && data.email.Length>1 && data.email.Contains('@'))
                        {
                            email = email + ";" + data.email;
                        }
                        bool sended = PAMC.EmailSender.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, email, null);
                    }
                    else
                    {
                        if (data.email != null && data.email != "" && data.email.Length > 1 && data.email.Contains('@'))
                        {
                            email = data.email;
                            bool sended = PAMC.EmailSender.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, email, null);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }
            return vr;
        }

        [HttpGet]
        [Route("GetDefaultCodes")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetDefaultCode()
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();

            return list;
        }
    
        [HttpGet]
        [Route("GetDefaultCodeGroups")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetDefaultCodeGroups()
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT id, Description, Enabled FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE Enabled = '1'";

                    dt.Load(cmd.ExecuteReader());

                    foreach(DataRow dr in dt.Rows)
                    {
                        DefaultCodeModel d = new DefaultCodeModel();

                        d.id = dr["id"].ToString();
                        d.heading = dr["Description"].ToString();

                        list.Add(d);
                       
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                throw;
            }
            return list;
        }
    
    
        [HttpPost]
        [Route("GetDefaultCodeDetails")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetDefaultCodeDetails([FromBody] DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    DataTable dtbl = new DataTable();
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));

                    cmd.CommandText = $"SELECT * FROM SVC_CODES_SETUP_DETAIL WHERE HeadId = @id";

                    

                    dt.Load(cmd.ExecuteReader());

                    cmd.CommandText = $"SELECT HPCODE,FEESET FROM MEDIWALLET_HP_SETUP WHERE ENABLED = '1'";
                    dtbl.Load(cmd.ExecuteReader());

                    string hp = dtbl.Rows[0][0].ToString();
                    string feeset = dtbl.Rows[0][1].ToString();
                    feeset = $"'{feeset.Replace(",","','")}'";

                    dtbl = new DataTable();
                    //And a.provid ='{data.provid}'

                    foreach (DataRow row in dt.Rows)
                    {
                        DefaultCodeModel d = new DefaultCodeModel();

                        d.code = row["Code"].ToString();
                        d.desc = row["Description"].ToString();
                        d.enterQty = Convert.ToBoolean(row["EnterQty"].ToString());
                        d.defaultQty = Convert.ToInt32(row["DefaultQty"].ToString());
                        d.enterTooth = Convert.ToBoolean(row["EnterToothNo"].ToString());
                        d.dependantOnCode = Convert.ToBoolean(row["DependantOnOtherCode"].ToString());
                        d.depCode = row["DependantCode"].ToString();
                        
                        dtbl = new DataTable();

                        cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET IN({feeset}) AND PHCODE = 'P' AND BEGPROC = '{d.code.PadLeft(20,' ')}' ";
                        dtbl.Load(cmd.ExecuteReader());
                        if(dtbl.Rows.Count > 0)
                        {
                            d.price = dtbl.Rows[0][0].ToString();
                        }
                        else
                        {
                            d.price = "";
                        }


                        list.Add(d);

                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }


            return list;
        }

        [HttpPost]
        [Route("GetAllClaims")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimCodesModel> GetAllClaims([FromBody] LoginDetailsModel data)
        {
            List<ClaimCodesModel> list = new List<ClaimCodesModel>();

            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddDays(-14).ToString("yyyy/MM/dd");

                    string sql = "SELECT cd.headId,ch.memberId,ch.uuid,ch.createDate,\n"
           + "SUM(CAST(ISNULL(cd.PayableByProv,'0.00') AS decimal(18,2))) AS [PayableByProv],\n"
           + "SUM(CAST(ISNULL(cd.PayableByMemb,'0.00')AS DECIMAL(18, 2))) AS [PayableByMemb],\n"
           + "SUM(CAST(ISNULL(cd.total,'0.00')AS DECIMAL(18, 2)))AS [total],ch.ref,ch.mediRef \n"
           + $" FROM {MediDatabase}.dbo.Claim_Header ch\n"
           + $"INNER JOIN {MediDatabase}.dbo.Claim_Detail cd on cd.headId = ch.id\n"
           + "GROUP BY  cd.headId,ch.memberId,ch.ref,ch.mediRef,ch.uuid,ch.createDate";

                    cmd.CommandText = sql;
                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        ClaimCodesModel cm = new ClaimCodesModel();

                        cm.id = row["headId"].ToString();
                        cm.desc = row["uuid"].ToString();
                        cm.membId = row["memberId"].ToString();
                        cm.total = row["total"].ToString();
                        cm.provpay = row["PayableByProv"].ToString();
                        cm.membpay = row["PayableByMemb"].ToString();
                        cm.qty = row["createDate"].ToString();
                        cm.mediRef = row["mediRef"].ToString();
                        cm.drcRef = row["ref"].ToString();

                        list.Add(cm);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return list;
        }


        [HttpPost]
        [Route("GetClaimDetail")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimCodesModel> GetClaimDetail([FromBody] ClaimCodesModel data)
        {
            List<ClaimCodesModel> list = new List<ClaimCodesModel>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddDays(-14).ToString("yyyy/MM/dd");

                    cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.Claim_Detail WHERE headId = '{data.id}'";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        ClaimCodesModel cm = new ClaimCodesModel();

                        cm.diag = row["DiagCode"].ToString();
                        cm.code = row["ProcCode"].ToString();
                        cm.toothNo = row["ToothNo"].ToString();
                        if(cm.toothNo == "" || cm.toothNo == null)
                        {
                            cm.toothNo = "N/A";
                        }
                        cm.qty = row["qty"].ToString();
                        cm.singleUnitAmount = row["singleUnitValue"].ToString();
                        cm.total = row["total"].ToString();
                        cm.membpay = row["PayableByMemb"].ToString();
                        cm.provpay = row["PayableByProv"].ToString();
                        list.Add(cm);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return list;
        }

    
        [HttpPost]
        [Route("ValidateToothNo")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateToothNo([FromBody] ClaimCodesModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    string[] tooth = data.toothNo.Split(',');

                    bool alltheethValid = false;
                    foreach (string toothno in tooth)
                    {
                        cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.CLINICAL_CODES WHERE CLINICALCODE = '{toothno}'";
                        DataTable dt = new DataTable();

                        dt.Load(cmd.ExecuteReader());

                        if (dt.Rows.Count > 0)
                        {
                            alltheethValid = alltheethValid ||  true;
                        }
                        else
                        {
                            alltheethValid = alltheethValid && false;
                        }
                    }

                    vr.valid = alltheethValid;
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpGet]
        [Route("GetSettingDiags")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DiagCodes> GetSettingDiags()
        {
            List<DiagCodes> list = new List<DiagCodes>();

            try
            {
                using(SqlConnection cn = new SqlConnection(base._mediConnection.ConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT SettingDesc FROM Settings WHERE SettingId = 'Diags' AND Enabled = 1";

                    dt.Load(cmd.ExecuteReader());

                    string diags = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        diags = row["SettingDesc"].ToString();
                    }

                    if (diags.Length>0)
                    {
                        string[] codes = new string[diags.Split(";").Length];

                        codes = diags.Split(";");

                        for (int i = 0; i < codes.Length; i++)
                        {
                            cmd.CommandText = $"SELECT DIAGDESC FROM {DRCDatabase}.dbo.DIAG_CODES WHERE DIAGCODE = '{codes[i]}'";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            foreach (DataRow item in dt.Rows)
                            {
                                DiagCodes d = new DiagCodes();
                                d.code = codes[i];
                                d.description = item["DIAGDESC"].ToString();
                                list.Add(d);
                            }

                        }
                    }


                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return list;
        }

        [HttpPost]
        public bool SaveDataToDAT(string id)
        {
            bool succes = false;
            string sex = "";
            string name = "";
            string surname = "";
            string dob = "";
            string idno = "";
          
            RecordType0 rec0 = new RecordType0();
            RecordType1 rec1 = new RecordType1();
            List<RecordType2> rec2 = new List<RecordType2>();
            RecordType5 rec5 = new RecordType5();


            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    rec0.TYPE = 0;
                    rec0.CONTROL = "BATCH";
                    rec0.DATECREATED = DateTime.Now.ToString("yyyy/MM/dd");

                    

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@date", rec0.DATECREATED));
                    cmd.Parameters.Add(new SqlParameter("@control", rec0.CONTROL));
                    cmd.Parameters.Add(new SqlParameter("@type", rec0.TYPE));


                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record0 (TYPE,CONTROL,FILLER1,PROVIDER,CLRHOUSE,FILLER3," +
                        $"DATECREATED,FILEER4,DATEACCESSED,BATCHNO,COMPLETED) " +
                        $" OUTPUT INSERTED.FileID " +
                        $" VALUES (@type,'','','','','',@date,'',NULL,NULL,0) ";

                    rec0.Rec0ID = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand();
                    cmd.Connection = cn;




                    cmd.Parameters.Add(new SqlParameter("@id", id));

                    cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.Claim_Header WHERE id = @id";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        rec1.Rec0ID = rec0.Rec0ID;
                        rec1.PLACE = "11";
                        rec1.PHCODE = "P";
                        rec1.EZCAPPROV = row["provid"].ToString();
                        rec1.VENDORID = row["provid"].ToString();
                        rec1.PROVCLAIM = row["ref"].ToString() + "-" + row["mediRef"].ToString();
                        rec1.EXT_SWITCH = "MEDIWALLETWEB";
                        rec1.CLAIMTYPE = "P";
                        rec1.CREATEBY = row["createby"].ToString();
                        rec1.DATERECD = row["createdate"].ToString().Substring(0,10);
                        idno = row["memberId"].ToString();
                        name = row["firstname"].ToString();
                        surname = row["lastname"].ToString();
                        dob = row["dateofbirth"].ToString();

                    }


                    cmd.CommandText = $"EXEC DRCJ.dbo.CreateMediWalletMemb '{surname}','{name}','{dob}','{idno}','{rec1.DATERECD}' ";
                    int success = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd.CommandText = $"SELECT MEMBID FROM {DRCJDatabase}.dbo.MEMB_MASTERS WHERE PATID = '{idno}' AND BIRTH = '{dob}'" +
                        $" AND HPCODE = 'MEDW' AND LASTNM = '{surname}'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    rec1.EZCAPMEMB = dt.Rows[0][0].ToString();
                    cmd.Parameters.Add(new SqlParameter("@type", 1));
                    cmd.Parameters.Add(new SqlParameter("@phcode", rec1.PHCODE));
                    cmd.Parameters.Add(new SqlParameter("@ref", rec1.PROVCLAIM));
                    cmd.Parameters.Add(new SqlParameter("@prov", rec1.EZCAPPROV));
                    cmd.Parameters.Add(new SqlParameter("@memb", rec1.EZCAPMEMB));
                    cmd.Parameters.Add(new SqlParameter("@place", rec1.PLACE));
                    cmd.Parameters.Add(new SqlParameter("@claimtype", rec1.CLAIMTYPE));
                    cmd.Parameters.Add(new SqlParameter("@vend", rec1.VENDORID));
                    cmd.Parameters.Add(new SqlParameter("@daterec", rec1.DATERECD));
                    cmd.Parameters.Add(new SqlParameter("@ext", rec1.EXT_SWITCH));
                    cmd.Parameters.Add(new SqlParameter("@fileid", rec1.Rec0ID));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd")));
                    cmd.Parameters.Add(new SqlParameter("@by", rec1.CREATEBY));

                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record1 (TYPE,PHCODE,PROVCLAIM,EZCAPUATH,EZCAPPROV" +
                        $",EZCAPMEMB,PLACE,OUTCOME,EDIREF,CASENUM,UNITS,CLAIMTYPE,REFPROVID,VENDORID,DATERECD,BATCH_NO,[EXT SWITCH]," +
                        $"FileID,CREATEDATE,CREATEBY) " +
                        $" OUTPUT INSERTED.Rec1ID " +
                        $" VALUES (@type,@phcode,@ref,'',@prov,@memb,@place,'','','','',@claimtype,'',@vend,@daterec,''," +
                        $"@ext,@fileid,@date,@by)";

                    rec1.Rec1ID = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand();
                    cmd.Connection = cn;
                    dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@id", id));

                    cmd.CommandText = $"SELECT * FROM  Claim_Detail WHERE headId = @id";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        RecordType2 r = new RecordType2();
                        rec5.DIAG = row["DiagCode"].ToString();
                        rec5.DIAGREF = 1;
                        rec5.TYPE = 5;
                        rec5.FieldID = rec1.Rec1ID;
                        r.TYPE = 2; ;
                        r.PHCODE = "P";
                        r.TODATE = row["CreateDate"].ToString().Substring(0, 10);
                        r.FROMDATE = row["CreateDate"].ToString().Substring(0,10);
                        r.PROCCODE = row["ProcCode"].ToString();
                        r.QTY = row["qty"].ToString();
                        r.BILLED = row["PayableByProv"].ToString();
                        r.DIAGREF = 1;
                        r.Rec1ID = rec1.Rec1ID;
                        if (DBNull.Value.Equals(row["ToothNo"]))
                        {
                            r.CLINICALCODE1 = "";
                            r.CLINICALCODE2 = "";
                            r.CLINICALCODE3 = "";
                            r.CLINICALCODE4 = "";
                            r.CLINICALCODE5 = "";
                            r.CLINICALCODE6 = "";
                            r.CLINICALCODE7 = "";
                            r.CLINICALCODE8 = "";
                        }
                        else
                        {
                            string[] tooths = new string[row["ToothNo"].ToString().Split(",").Length];
                            tooths = row["ToothNo"].ToString().Split(",");
                            if(tooths.Length == 1)
                            {
                                r.CLINICALCODE1 = tooths[0];
                            }
                            else if (tooths.Length == 2)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2= tooths[1];
                            }
                            else if (tooths.Length == 3)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2 = tooths[1];
                                r.CLINICALCODE3 = tooths[2];
                            }
                            else if (tooths.Length == 4)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2 = tooths[1];
                                r.CLINICALCODE3 = tooths[2];
                                r.CLINICALCODE4 = tooths[3];
                            }
                            else if (tooths.Length == 5)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2 = tooths[1];
                                r.CLINICALCODE3 = tooths[2];
                                r.CLINICALCODE4 = tooths[3];
                                r.CLINICALCODE5 = tooths[4];
                            }
                            else if (tooths.Length == 6)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2 = tooths[1];
                                r.CLINICALCODE3 = tooths[2];
                                r.CLINICALCODE4 = tooths[3];
                                r.CLINICALCODE5 = tooths[4];
                                r.CLINICALCODE6 = tooths[5];
                            }
                            else if (tooths.Length == 7)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2 = tooths[1];
                                r.CLINICALCODE3 = tooths[2];
                                r.CLINICALCODE4 = tooths[3];
                                r.CLINICALCODE5 = tooths[4];
                                r.CLINICALCODE6 = tooths[5];
                                r.CLINICALCODE7 = tooths[6];
                            }
                            else if (tooths.Length == 8)
                            {
                                r.CLINICALCODE1 = tooths[0];
                                r.CLINICALCODE2 = tooths[1];
                                r.CLINICALCODE3 = tooths[2];
                                r.CLINICALCODE4 = tooths[3];
                                r.CLINICALCODE5 = tooths[4];
                                r.CLINICALCODE6 = tooths[5];
                                r.CLINICALCODE7 = tooths[6];
                                r.CLINICALCODE8 = tooths[7];
                            }


                        }

                        cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record2 (TYPE,PHCODE,FROMDATE,TODATE,PROCCODE," +
    $"CURR_MODCODE,CURR_MODAMOUNT,MODCODE,MODAMOUNT,MODCODE2,MODAMOUNT2,MODCODE3,MODAMOUNT3,MODCODE4," +
    $"MODAMOUNT4,QTY,BILLED,DIAGREF,BUNDLER,BUNDLERTYP,COPAY,MANDISCOUNT,TRANSACT_NO,PROCCODE_DESC," +
    $"CLINICALCODE1,CLINICALCODE2,CLINICALCODE3,CLINICALCODE4,CLINICALCODE5,CLINICALCODE6,CLINICALCODE7," +
    $"CLINICALCODE8,MEMO1,MEMO2,MEMO3,MEMO4,MEMO5,MEMO6,MEMO7,MEMO8,MEMO9,MEMO10,Rec1ID)" +
    $" VALUES ('{r.TYPE}','{r.PHCODE}','{r.FROMDATE}','{r.TODATE}','{r.PROCCODE}','','','','','','','','','','','{r.QTY}'" +
    $",'{r.BILLED}'," +
    $"'{r.DIAGREF}','','','','','','','{r.CLINICALCODE1}','{r.CLINICALCODE2}','{r.CLINICALCODE3}'," +
    $"'{r.CLINICALCODE4}','{r.CLINICALCODE5}'," +
    $"'{r.CLINICALCODE6}','{r.CLINICALCODE7}','{r.CLINICALCODE8}','','','',''," +
    $"'','','','','','','{r.Rec1ID}')";
                        cmd.ExecuteNonQuery();


                    }
                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record5 (TYPE,DIAGREF,DIAG,Rec1ID) " +
                        $"VALUES ('5','1','{rec5.DIAG}','{rec5.FieldID}')";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}..Record0 set COMPLETED = 1 WHERE FileID = '{rec0.Rec0ID}'";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return succes;
        }

        public static void LogError(string error, string stacktrace)
        {

            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "CLAIMCAPTURE";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\{fileName}");
            // fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }



    }
}