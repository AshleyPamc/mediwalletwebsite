﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MediWalletWebsite_1.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IO;
using System.Security.Claims;

namespace MediWalletWebsite_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : Base.ContextController
    {
        private string fileName = "app.txt";

        public TokenController(IWebHostEnvironment env, IConfiguration con):base(env,con)
        {

        }

        [HttpPost("token")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ActionResult GetToken([FromBody] LoginDetailsModel data)
        {
            var token = new JwtSecurityToken();
            var root = _env.WebRootPath;
            var roles = new List<Claim>();
            //  roles.Add(new Claim(ClaimTypes.Role, "Administrator"));
            // roles.Add(new Claim(ClaimTypes.Role, "NormalUser"));
            DirectoryInfo di = new DirectoryInfo($"{root}\\tmp");
            FileInfo f = new FileInfo($"{root}\\tmp\\{ fileName }");
            string key;


            //security key
            using (StreamReader reader = new StreamReader(f.FullName))
            {
                key = reader.ReadLine();
                reader.Close();
            }

            //symetric security key


            var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            //signing credentials

            var signingCredentials = new SigningCredentials(symetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            //Adding Roles


            //create token
            if (data.userType == 0)
            {
                //if (_allowAdmin)
                //{

                //    roles.Add(new Claim(ClaimTypes.Role, "Administrator"));
                //    token = new JwtSecurityToken(
                //    issuer: "pamcportal.in",
                //    audience: "authorizedUsers",
                //    expires: DateTime.Now.AddHours(8),
                //    signingCredentials: signingCredentials,
                //    claims: roles
                //    );
                //}
//else
               // {
                    roles.Add(new Claim(ClaimTypes.Role, "NormalUser"));
                    token = new JwtSecurityToken(
                        issuer: "pamcportal.in",
                        audience: "authorizedUsers",
                        expires: DateTime.Now.AddHours(8),
                        signingCredentials: signingCredentials,
                        claims: roles
                   );
                //}
            }
            else
            {
                roles.Add(new Claim(ClaimTypes.Role, "NormalUser"));
                token = new JwtSecurityToken(
                    issuer: "pamcportal.in",
                    audience: "authorizedUsers",
                     expires: DateTime.Now.AddHours(8),
                    signingCredentials: signingCredentials,
                    claims: roles
               );
            }


            //return token
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));

        }

    }
}