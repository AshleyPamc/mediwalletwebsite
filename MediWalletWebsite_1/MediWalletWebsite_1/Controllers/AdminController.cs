﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MediWalletWebsite_1.Models;
using System.Data.SqlClient;
using System.Data;

namespace MediWalletWebsite_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Base.ContextController
    {

        public AdminController(IWebHostEnvironment env, IConfiguration con): base(env, con)
        {

        }

        [HttpGet]
        [Route("GetProcCodes")]

        public List<DefaultCodeModel> GetProcCodes()
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                using(SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if(cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT SVCCODE,SVCDESC,PHCODE FROM {DRCDatabase}.dbo.SERVICE_CODES WHERE PHCODE = 'P'";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach(DataRow dr in dt.Rows)
                    {
                        DefaultCodeModel d = new DefaultCodeModel();
                        d.code = dr["SVCCODE"].ToString().Trim(' ');
                        d.desc = dr["SVCDESC"].ToString();
                        d.phcode = dr["PHCODE"].ToString();

                        list.Add(d);
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return list;
        }


        [HttpGet]
        [Route("GetExistingCodeGroups")]

        public List<DefaultCodeModel> GetExistingCodeGroups()
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow dr in dt.Rows)
                    {
                        DefaultCodeModel d = new DefaultCodeModel();
                        d.heading = dr["Description"].ToString();
                        d.enabled = Convert.ToBoolean(dr["Enabled"].ToString());
                        d.id = dr["id"].ToString();

                        list.Add(d);
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return list;
        }
    
        [HttpPost]
        [Route("DeleteExistingCodeGroup")]
        public ValidationResultsModel DeleteExistingCodeGroup([FromBody] DefaultCodeModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            using(SqlConnection cn = new SqlConnection(_mediConnectionString))
            {
                if(cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();

                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@id", data.id));

                cmd.CommandText = $"DELETE FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE id = @id";
                cmd.ExecuteNonQuery();

                cmd.CommandText = $"DELETE FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL WHERE HeadId = @id";
                cmd.ExecuteNonQuery();
            }

            return vr;
        }

        [HttpPost]
        [Route("GetExistingCodeGroupsDetail")]

        public List<DefaultCodeModel> GetExistingCodeGroupsDetail([FromBody]DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;


                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL WHERE headId = @id";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow dr in dt.Rows)
                    {
                        DefaultCodeModel d = new DefaultCodeModel();
                        d.code = dr["Code"].ToString().Trim(' ');
                        d.desc = dr["Description"].ToString();
                        d.enterQty = Convert.ToBoolean(dr["EnterQty"].ToString());
                        d.defaultQty = Convert.ToInt32(dr["DefaultQty"].ToString());
                        d.enterTooth = Convert.ToBoolean(dr["EnterToothNo"].ToString());
                        d.dependantOnCode = Convert.ToBoolean(dr["DependantOnOtherCode"].ToString());
                        d.depCode = dr["DependantCode"].ToString();

                        list.Add(d);
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return list;
        }
    

        [HttpPost]
        [Route("AddNewCode")]

        public ValidationResultsModel  AddNewCode([FromBody] DefaultCodeModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@des", data.heading));
                    cmd.Parameters.Add(new SqlParameter("@enabled", data.enabled));
                    cmd.Parameters.Add(new SqlParameter("@user", data.user));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")));

                    cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER (Description,Enabled,CreateDate,CreateBy,ChangeDate,ChangeBy)\n" +
                        $"OUTPUT INSERTED.id \n " +
                        $"VALUES (@des,@enabled,@date,@user,@date,@user)";
                    vr.message = cmd.ExecuteScalar().ToString();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return vr;
        }


        [HttpPost]
        [Route("AddNewCodeDetail")]

        public ValidationResultsModel AddNewCodeDetail([FromBody] DefaultCodeModel[] data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@user", data[0].user));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));

                    foreach(DefaultCodeModel code in data)
                    {
                        cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL (HeadId,Code,Description,EnterQty,DefaultQty," +
                            $"EnterToothNo,DependantOnOtherCode,DependantCode,CreateDate,CreateBy,ChangeDate,ChangeBy)\n" +
                                           $"VALUES ('{code.id}','{code.code}','{code.desc}','{code.enterQty}','{code.defaultQty}','{code.enterTooth}'," +
                                           $"'{code.dependantOnCode}','{code.depCode}',@date,@user,@date,@user)";
                        cmd.ExecuteNonQuery();
                    }

                   
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return vr;
        }
    
    
        [HttpGet]
        [Route("GetHpSetup")]

        public List<HpSetup> GetHpSetup()
        {
            List<HpSetup> list = new List<HpSetup>();
            try
            {
                using(SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.MEDIWALLET_HP_SETUP";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        HpSetup ph = new HpSetup();
                        ph.hpcode = row["HPCODE"].ToString();
                        ph.descr = row["DESCR"].ToString();
                        ph.id = Convert.ToInt32(row["id"].ToString());
                        ph.enabled = Convert.ToBoolean(row["ENABLED"].ToString());

                        list.Add(ph);
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
            return list;
        }

        [HttpPost]
        [Route("UpdateHpSetup")]

        public ValidationResultsModel UpdateHpSetup([FromBody] HpSetup data)
        {
            ValidationResultsModel list = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = $"UPDATE {MediDatabase}.dbo.MEDIWALLET_HP_SETUP SET HPCODE = '{data.hpcode}', DESCR = '{data.descr}', ENABLED = '{data.enabled}'," +
                        $" CHANGEDATE = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")}', CHANGEBY = '{data.user}' WHERE id = '{data.id}'";

                    cmd.ExecuteNonQuery();


                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
            return list;
        }

        [HttpPost]
        [Route("DeleteHpSetup")]

        public ValidationResultsModel DeleteHpSetup([FromBody] HpSetup data)
        {
            ValidationResultsModel list = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = $"DELETE FROM {MediDatabase}.dbo.MEDIWALLET_HP_SETUP  WHERE id = '{data.id}'";

                    cmd.ExecuteNonQuery();


                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
            return list;
        }


        [HttpPost]
        [Route("NewHpSetup")]

        public List<HpSetup> NewHpSetup([FromBody] HpSetup data)
        {
            List<HpSetup> list = new List<HpSetup>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;


                    cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.MEDIWALLET_HP_SETUP ( HPCODE , DESCR , ENABLED ," +
                        $" CHANGEDATE,CREATEDATE, CHANGEBY,CREATEBY) \n" +
                        $"VALUES('{data.hpcode}','{data.descr}','{data.enabled}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'" +
                        $",'{data.user}','{data.user}')";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
            return list;
        }


    }
}