﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;

namespace MediWalletWebsite_1.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContextController : ControllerBase
    {
        public static IWebHostEnvironment _env;
        public IConfiguration _configuration;
        public static string _drcConnectionString;
        //public static string _trainingDataConnectionStrting;
        public static string _portalConnectionString;
        public static string _drcjConnectionString;
        public static string _mediConnectionString;
        public static string _FeverTreeSvcConnection;
        internal SqlConnection _drcConnection = null;
        internal SqlConnection _pamcPortalConnection = null;
        internal SqlConnection _mediConnection = null;
        internal SqlConnection _drcjConnection = null;
        public ContextController(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
            var connections = _configuration.GetSection("ConnectionStrings").GetChildren().AsEnumerable();
            var webCon = _configuration.GetSection("WebConnections").GetChildren().AsEnumerable();
            _drcConnectionString = connections.ToArray()[0].Value;
            _portalConnectionString = connections.ToArray()[3].Value;
            _drcjConnectionString = connections.ToArray()[1].Value;
            _mediConnectionString = connections.ToArray()[2].Value;
            _FeverTreeSvcConnection = webCon.ToArray()[0].Value;
            this._drcConnection = new SqlConnection(_drcConnectionString);
            this._pamcPortalConnection = new SqlConnection(_portalConnectionString);
            this._mediConnection = new SqlConnection(_mediConnectionString);
            _drcjConnection = new SqlConnection(_drcjConnectionString);
        }

        public string DRCDatabase
        {
            get
            {
                return this._drcConnection.Database;
            }
        }
        public string PamcPortalDatabase
        {
            get
            {
                return this._pamcPortalConnection.Database;
            }
        }

        public string MediDatabase
        {
            get
            {
                return this._mediConnection.Database;
            }
        }
        public string DRCJDatabase
        {
            get
            {
                return this._drcjConnection.Database;
            }
        }
    }



}