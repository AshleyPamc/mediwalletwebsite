﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using MediWalletWebsite_1.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace MediWalletWebsite_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Base.ContextController
    {
        public LoginController(IWebHostEnvironment env, IConfiguration configuration) : base(env, configuration)
        {

        }

        [HttpGet]
        [Route("TestConnection")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel TestConnection()
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                bool drcCon = false;
                bool pamcCon = false;
                bool medi = false;

                using (SqlConnection drc = new SqlConnection(_drcConnectionString))
                {

                    if (drc.State != System.Data.ConnectionState.Open)
                    {
                        try
                        {
                            drc.Open();
                            drcCon = true;
                            drc.Close();
                        }
                        catch (SqlException e)
                        {

                            drcCon = false;
                        }


                    }



                }
                using (SqlConnection pamc = new SqlConnection(_portalConnectionString))
                {

                    if (pamc.State != System.Data.ConnectionState.Open)
                    {
                        try
                        {
                            pamc.Open();
                            pamcCon = true;
                            pamc.Close();
                        }
                        catch (SqlException e)
                        {

                            pamcCon = false;
                        }

                    }
                }
                using (SqlConnection medicon = new SqlConnection(_mediConnectionString))
                {

                    if (medicon.State != System.Data.ConnectionState.Open)
                    {
                        try
                        {
                            medicon.Open();
                            medi = true;
                            medicon.Close();
                        }
                        catch (SqlException e)
                        {

                            medi = false;
                        }

                    }




                }

                if (pamcCon && drcCon && medi)
                {
                    vr.valid = true;
                }
                else
                {
                    vr.valid = false;
                }

            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
            vr.message = ".";
            return vr;
        }
    
        [HttpPost]
        [Route("ComfirmLogin")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel ComfirmLogin([FromBody] LoginDetailsModel data)
        {

            try
            {
                using(SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if(cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@password", data.password));
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));

                    if(data.username.ToUpper() != "ADMIN")
                    {
                        cmd.CommandText = $"SELECT u.userType,u.provId,pm.LASTNAME AS name,sp.SPECCODE FROM {PamcPortalDatabase}.dbo.Users u \n" +
                                          $"INNER JOIN {DRCDatabase}.dbo.PROV_MASTERS pm ON pm.PROVID = u.PROVID \n" +
                                          $"INNER JOIN {DRCDatabase}.dbo.PROV_SPECINFO sp ON sp.PROVID = pm.PROVID \n" +
                                          $" WHERE username = @username AND password = @password";
                    }
                    else
                    {
                        cmd.CommandText = $"SELECT u.userType,u.provId,u.name AS name FROM {PamcPortalDatabase}.dbo.Users u \n" +
                                         $" WHERE username = @username AND password = @password";
                    }


                    dt.Load(cmd.ExecuteReader());

                    if(dt.Rows.Count > 0)
                    {
                        foreach(DataRow row in dt.Rows)
                        {
                            data.userType = Convert.ToInt32(row["userType"].ToString());
                            data.provID =  row["provId"].ToString(); 
                            data.Lastname = row["name"].ToString();
                            data.success = true;
                            if (data.username.ToUpper() != "ADMIN")
                            {
                            data.SpecCode = row["SPECCODE"].ToString();
                            
                                if ((data.SpecCode == "14") || (data.SpecCode == "15"))
                                {
                                    data.dentist = false;
                                    data.gp = true;
                                }
                                if ((data.SpecCode == "95") || (data.SpecCode == "54"))
                                {
                                    data.dentist = true;
                                    data.gp = false;
                                }
                            }

                        }
                    }
                    else
                    {
                        data.success = false;
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }


            return data;
        }

        [HttpPost]
        [Route("UpdateLogin")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel UpdateLogin([FromBody] LoginDetailsModel data)
        {

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@password", data.password));
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@new", data.newPassword));

                    cmd.CommandText = $"Update {PamcPortalDatabase}.dbo.Users SET password = @new, changeBy = @username, " +
                        $"changeDate = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")}'  WHERE username = @username AND password = @password";

                    cmd.ExecuteNonQuery();

                    data.newPassword = data.newPassword;
                }
            }
            catch (Exception e)
            {

                throw;
            }


            return data;
        }

        [HttpPost]
        [Route("GetLinkedEmails")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Emails> GetLinkedEmails([FromBody] LoginDetailsModel data)
        {
            List<Emails> list = new List<Emails>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT email FROM {MediDatabase}.dbo.EmailListPerProvider WHERE provid = '{data.provID}'";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Emails email = new Emails();

                        email.email = row["email"].ToString();

                        list.Add(email);

                        
                    }
                }
            }
            catch (Exception ex)
            {

                var msg = ex.Message;
            }

            return list;
        }

    }
}